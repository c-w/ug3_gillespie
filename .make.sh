#!/bin/bash

function generate_docs() {
    local doxygen_dir=$doc_dir/doxygen
    {
    echo "PROJECT_NAME = \"$project_name\""
    echo "PROJECT_BRIEF = \"A simulator implementing the Gillespie algorithm\""
    echo "OUTPUT_DIRECTORY = $doxygen_dir"
    echo "HTML_OUTPUT = ."
    echo "JAVADOC_AUTOBRIEF = YES"
    echo "EXTRACT_ALL = YES"
    echo "EXTRACT_PRIVATE = YES"
    echo "EXTRACT_STATIC = YES"
    echo "EXTRACT_LOCAL_CLASSES = YES"
    echo "EXTRACT_LOCAL_METHODS = YES"
    echo "SHOW_DIRECTORIES = YES"
    echo "QUIET = YES"
    echo "WARNINGS = NO"
    echo "INPUT = src/"
    echo "RECURSIVE = YES"
    echo "GENERATE_LATEX = NO"
    } > doxygenConfig
    rm -rf $doxygen_dir &> /dev/null
    mkdir -p $doxygen_dir &> /dev/null
    doxygen doxygenConfig
    if [ $? -gt 0 ]; then
        echo "$script_name: problem with doxygen-call"
        exit
    fi
    local favicon_html='<link rel="icon" type="img/png" href="data:image/png;base64,AAABAAEAEBAAAAEAIABoBAAAFgAAACgAAAAQAAAAIAAAAAEAIAAAAAAAAAQAAAAAAAAAAAAAAAAAAAAAAAD///8A7u7uEv39/QH19fUJ7u7uEpiVsnluZruxQ0SO4Dw2e+loX7i5lZG3furq6hfz8/MK9/f3B////wD///8A////AP///wDx8fIOn5+kbS4+ivctQbP/LEec/1J2iv8+X3T/J0CM/ys/of8uLXP4nJukcPv7+wPz8/MK////AP///wD///8AeIWMl2xvU/+FfXD/9/f3/6uqrP8/PkH/MzI2/6Wkpv/j5uf/anNp/0heVP93d4Ka////APHx8Q7y8fIPpKG6aytEp//Cdij/vnUn/56Nfv/d3d3/TExP/0xLTv/T09T/l4Nx/791J/+hdjv/LDGQ/6GewW75+fkG8PDwDzors/csQq3/wnYo/8h6KP+8cyf/Y1JE/5CPkf+OjY//ZlE+/8F2J//Ieij/pHc6/y0zmf85K7347u7uE5yZtHQ8Ke3/LESx/62HYP/Ieij/yHoo/710J/+ci3z/moVx/8F2J//Ieij/xXgo/4t/Z/8tM5r/PSnu/5qWunl6csCjPSnu/yxFtP/t7e3/uZJq/6JlJv8/eXr/P3uB/0V+gf9OZ1v/sG0n/6mOcv/U4uX/LTWc/z0p7v90a8KqX1W4xj0p7v8tQ7T/8vLy//r6+v+RfGn/T7vU/268zv90yNv/RpKk/5eEcf/8/Pz/1+Xo/yw4nv89Ke7/V0u20F1TtMY9Ke7/LUO4/+/v8P/6+vr/dW1n/0650f9vvdD/c8bZ/0mftP+FeW7//Pz8/9fl6f8sOJ7/PSnu/1JHtNd/d8KcPSnu/y1Ct//o6Oj/lX5n/55jJv9QrsP/abLC/2uywv9VlZ//r2wn/5KAb//W4eT/LDmg/z0p7v9yasKsp6W6ZTwp7P8uQbf/iXFa/8R3KP/Ieij/sHQx/4BuW/+IcFf/u3Ur/8h6KP+/dSf/eXFh/yw5n/89Ke3/m5e2d/f39wdFN7vpLkO6/7VwJ//Ieij/xXgo/7acgf9sdGD/V1xQ/7uegP/GeSj/yHoo/6h0Nf8sO6D/Oy2y9e/v8A/y8vIPuLbKUitCp/+0cSr/wnYn/7GXff/PxtX/YCKL/0YfY//Fvsr/uZx//8R4KP+kcjT/KjqT/6Sit2n39/cH7OzsFPPz9AqZoKZxKUKX/yxDsP8sRbP/LUGu/y1Dsv8tQbP/LUG1/y1DtP8tQrH/KkKj/32CiJL9/f0D8vHyD/n5+Qb///8A////ALu5yU1GOLLnPCjp/z0p7v89Ke7/PSnu/z0p7v88Kez/QTS27aypwF79/f0B5+bnGf39/QH///8A////AP39/QDz8/MM8PDwEaeluGV9db+fX1W3xmJYusGAecWaqKW7Z/X19Qrw8PAP6urqFf///wD///8A/D8AAPAPAADAAwAAwAMAAIABAACAAQAAAAAAAAAAAAAAAAAAAAAAAIABAACAAQAAwAMAAOADAADwDwAA/D8AAA==" />'
    for file in $(find $doxygen_dir -name '*.html'); do
        sed -i "/<head>/ a\ $favicon_html" $file
    done
    rm doxygenConfig
}

function run_on_examples() {
    local fun="obj/$project_name-cli"
    if [ ! -f $fun ]; then
        echo "$script_name: Need to build cli tool before making examples."
        exit
    fi
    local sample_dir=$doc_dir/sample_runs
    mkdir -p $sample_dir &> /dev/null
    rm $sample_dir/*.csv $sample_dir/*.png $sample_dir/*.txt &> /dev/null
    for infile in $examples_dir/*; do
        echo "Running project on $infile"
        local outfile="$sample_dir/$(basename $infile .txt).csv"
        $fun -i "$infile" -o "$outfile" -p 
        cp $infile $sample_dir/
    done
}

function install_ddcli() {
    local ddcli_dir=$lib_dir/ddcli
    local ddcli_lib=$lib_dir/libddcli.a
    if [ ! -f $ddcli_lib ] || [ ! -d $ddcli_dir ]; then
        echo "I require libddcli but it is not present"
        if [ ! -d $ddcli_dir ]; then
            echo "Downloading libddcli."
            local sc=$(wget -q -O - http://www.dribin.org/dave/software)
            local url=$(echo $sc | sed 's/.*\(http:\/\/.*ddcli.*\.tgz\).*/\1/')
            wget -q $url
            if [ $? -gt 0 ]; then
                echo "$script_name: problem with wget call on ddcli"
                exit
            fi
            local tgz=$(basename $url)
            local dir=$(basename $url .tgz)
            tar -xf $tgz
            if [ $? -gt 0 ]; then
                echo "$script_name: problem with tar call on ddcli"
                exit
            fi
            mkdir -p $ddcli_dir
            mv $dir/lib/* $ddcli_dir
            rm $tgz
            rm -rf $dir
        fi
        if [ ! -f $ddcli_lib ]; then
            echo "Building libddcli as static library."
            local h=$(printf "%s " $(find $ddcli_dir -name '*.h'))
            local m=$(printf "%s " $(find $ddcli_dir -name '*.m'))
            {
            echo "LIBRARY_NAME = libddcli"
            echo "libddcli_OBJC_FILES = $m"
            echo "libddcli_HEADER_FILES = $h"
            echo "GNUSTEP_MAKEFILES = /usr/share/GNUstep/Makefiles"
            echo "include \$(GNUSTEP_MAKEFILES)/common.make"
            echo "-include GNUmakefile.preamble"
            echo "include \$(GNUSTEP_MAKEFILES)/library.make"
            echo "-include GNUmakefile.postamble"
            } > $ddcli_dir/GNUmakefile
            make -C $ddcli_dir shared=no > /dev/null
            if [ $? -gt 0 ]; then
                echo "$script_name: problem with make call on ddcli"
                exit
            fi
            rm $ddcli_dir/GNUmakefile
            mv $ddcli_dir/obj/libddcli.a $lib_dir
            rm -rf $ddcli_dir/obj
        fi
    fi
}

function install_gnuplot_i() {
    local gnuplot_i_dir=$lib_dir/gnuplot_i
    local gnuplot_i_lib=$lib_dir/libgnuplot_i.a
    if [ ! -f $gnuplot_i_lib ] || [ ! -d $gnuplot_i_dir ]; then
        echo "I require libgnuplot_i but it is not present"
        if [ ! -d $gnuplot_i_dir ]; then
            echo "Downloading libgnuplot_i"
            local url="http://ndevilla.free.fr/gnuplot/gnuplot_i-2.11.tar.gz"
            wget -q $url
            if [ $? -gt 0 ]; then
                echo "$script_name: problem with wget call on gnuplot_i"
                exit
            fi
            local tgz=$(basename $url)
            local dir=gnuplot_i
            tar -xf $tgz
            if [ $? -gt 0 ]; then
                echo "$script_name: problem with tar call on gnuplot_i"
                exit
            fi
            mkdir -p $gnuplot_i_dir
            mv $dir/src/* $gnuplot_i_dir
            rm $tgz
            rm -rf $dir
        fi
        if [ ! -f $gnuplot_i_lib ]; then
            echo "Building libgnuplot_i as static library."
            gcc -c $gnuplot_i_dir/*.c > /dev/null
            if [ $? -gt 0 ]; then
                echo "$scrit_name: problem with gcc call on gnuplot_i"
                exit
            fi
            ar rvs $gnuplot_i_lib -o *.o &> /dev/null
            if [ $? -gt 0 ]; then
                echo "$scrit_name: problem with ar call on gnuplot_i"
                exit
            fi
            rm *.o
        fi
    fi
}

function install_dependencies() {
    if [ "$do_apt_get" = "yes" ]; then
        sudo apt-get -y install tar make wget sed
        sudo apt-get -y install imagemagick
        sudo apt-get -y install gnustep gnustep-devel
        sudo apt-get -y install gobjc
        sudo apt-get -y install clang
        sudo apt-get -y install git
        sudo apt-get -y install gnuplot-x11
        sudo apt-get -y install doxygen
    fi
    install_ddcli
    install_gnuplot_i
}

function make_project() {
    if [ "x$project_type" = "x" ]; then
        echo "$script_name: Please specify tool-version to build (cli or gui)"
        exit
    fi
    local pch=$(ls $src_dir/*.pch)
    local h=$(printf "%s " $(find $project_dirs -type f -iname '*.h'))
    local m=$(printf "%s " $(find $project_dirs -type f -iname '*.m'))
    local lib=
    for libfile in $(ls $lib_dir/lib*.a); do
        lib="$lib -l$(basename $libfile | sed 's/lib\(.*\)\.a/\1/')"
    done
    local res=$(printf "%s " $(find $app_res_dir -type f -iname '*.jpeg'))
    local app_icon_name=$app_res_dir/Icon_48.jpeg
    {
    if [ "$project_type" = "tool" ]; then
        echo "TOOL_NAME = $project_name"
    elif [ "$project_type" = "application" ]; then
        echo "APP_NAME = $project_name"
        echo -n $project_name; echo "_APPLICATION_ICON = $app_icon_name"
        echo -n $project_name; echo "_RESOURCE_FILES = $res"
    fi
    echo -n $project_name; echo "_OBJC_PRECOMPILED_HEADERS = $pch"
    echo -n $project_name; echo "_HEADERS = $h"
    echo -n $project_name; echo "_OBJC_FILES = $m"
    echo -n $project_name; echo "_OBJCFLAGS += -include $pch"
    echo -n $project_name; echo "_OBJC_LIBS = $lib"
    echo "ADDITIONAL_LIB_DIRS = -L$lib_dir"
    echo "ADDITIONAL_INCLUDE_DIRS = -I$lib_dir"
    echo "GNUSTEP_MAKEFILES = $gnustep_makefiles"
    echo "include \$(GNUSTEP_MAKEFILES)/common.make"
    echo "-include GNUmakefile.preamble"
    echo "include \$(GNUSTEP_MAKEFILES)/$project_type.make"
    echo "-include GNUmakefile.postamble"
    } > GNUmakefile
    $make $make_args
    if [ $? -gt 0 ]; then
        echo "$script_name: problem with make call on project"
        exit
    fi
    rm GNUmakefile
    if [ "$project_type" = "tool" ]; then
        mv obj/$project_name obj/$project_name-cli
    elif [ "$project_type" = "application" ]; then
        rm -rf obj/$project_name.app
        mv $project_name.app obj/
        launcher="openapp \$(dirname \$(readlink -f \$0))/$project_name.app"
        echo $launcher > obj/cslp-gui
        chmod +x obj/cslp-gui
    fi
}
