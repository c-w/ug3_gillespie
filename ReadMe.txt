A stochastic discrete-event simulator in Objective-C.
See doc/handout.pdf for full detail.

Dependencies:
    compilation:
        gobjc
        gnustep
    static analysis:
        clang
    plotting:
        gnuplot
        imagemagick
    docs:
        doxygen
    version control:
        git
    libraries:
        ddcli (http://www.dribin.org/dave/software/#ddcli)
        gnuplot_i (http://ndevilla.free.fr/gnuplot/)

Build:
    $ ./make.sh dependencies
    $ ./make.sh cli
    $ ./make.sh gui
    $ ./make.sh docs

Run:
    $ obj/cslp-cli
    $ obj/cslp-gui
