# cascade with many molecule species

t = 100

A = 1000
B = 0
C = 0
D = 0
E = 0
F = 0
G = 0
H = 0
I = 0
J = 0
K = 0
L = 0
M = 0
N = 0
O = 0
P = 0
Q = 0
R = 0
S = 0

a : A -> B
b : B -> C
c : C -> D
d : D -> E
e : E -> F
f : F -> G
g : G -> H
h : H -> I
i : I -> J
j : J -> K
k : K -> L
l : L -> M
m : M -> N
n : N -> O
o : O -> P
p : P -> Q
q : Q -> R
r : R -> S

a = 0.5
b = 0.5
c = 0.5
d = 0.5
e = 0.5
f = 0.5
g = 0.5
h = 0.5
i = 0.5
j = 0.5
k = 0.5
l = 0.5
m = 0.5
n = 0.5
o = 0.5
p = 0.5
q = 0.5
r = 0.5
