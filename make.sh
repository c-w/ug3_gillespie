#!/bin/bash

# Script to automate:
#   - resolving project dependencies
#   - building the project
#   - generating documentation
#   - running project on sample scripts
# Helper functions are declared in .make.sh

gnustep_makefiles=/usr/share/GNUstep/Makefiles

# You shouldn't have to modify anything after this comment

script_name=$(basename $0)
tld_dir=$(dirname $(readlink -f $0))
lib_dir=$tld_dir/lib
src_dir=$tld_dir/src
doc_dir=$tld_dir/doc
res_dir=$tld_dir/res
examples_dir=$res_dir/examples
app_res_dir=$res_dir/app
project_name=cslp

source $tld_dir/.make.sh

function print_usage() {
    echo "$script_name: Usage [OPTIONS] PROJECT_VERSION MAKE_ARGS"
    echo ""
    echo "Compilation options - other arguments will be passed on to 'make'"
    echo "    cli                      build CLI version of project"
    echo "    gui                      build GUI version of project"
    echo "    -a, --static_analysis    compile with clang instead of gcc"
    echo ""
    echo "Other options - will exit script and ignore any other arguments"
    echo "    dependencies    try to automatically resolve all dependencies"
    echo "    examples        run cli version of project on all examples"
    echo "    docs            generate documentation with doxygen"
    echo "    -h, --help      print this help message"
}

# process cli arguments
make=make
make_args=
project_type=
project_dirs=$src_dir/shared
do_apt_get=yes
do_dependencies=no
do_docs=no
do_examples=no
for arg in "$@"; do
    case $arg in
        -a|--static_analysis)
            rm -rf obj
            make="scan-build make"
            ;;
        cli)
            project_type=tool
            project_dirs="$project_dirs $src_dir/cli"
            ;;
        gui)
            project_type=application 
            project_dirs="$project_dirs $src_dir/gui"
            ;;
        examples)
            do_examples=yes
            ;;
        docs)
            do_docs=yes
            ;;
        bare)
            do_apt_get=no
            ;;
        dependencies)
            do_dependencies=yes
            ;;
        -h|--help)
            print_usage
            exit
            ;;
        *)
            make_args="$make_args $arg"
            ;;
    esac
done

if [ "$do_dependencies" = "yes" ]; then
    install_dependencies
    exit
fi

if [ "$do_examples" = "yes" ]; then
    run_on_examples
    exit
fi

if [ "$do_docs" = "yes" ]; then
    generate_docs
    exit 
fi

make_project
