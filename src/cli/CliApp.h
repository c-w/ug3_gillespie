#import <ddcli/DDCommandLineInterface.h>
#import "../shared/simulator/SimulatorHeaders.h"

/**
 * A class offering a command-line interface to the simulator.
 */
@interface CliApp : NSObject <DDCliApplicationDelegate> {
    BOOL _test;
    BOOL _debug;
    BOOL _plot;
    BOOL _no_warnings;
    BOOL _help; 
    NSString* _input;
    NSString* _output;
    NSNumber* _timestep;
}

/**
 * Runs all the unit-tests for the project.
 *
 * @returns EXIT_SUCCESS if all tests passed or EXIT_FAILURE otherwise
 */
- (int)runTests;

/**
 * Run the simulation.
 *
 * @returns EXIT_SUCCESS if the simulation ran or EXIT_FAILURE otherwise
 */
- (int)runSimulator;

/**
 * Print the project help message/manpage to stdout.
 */
- (void)printHelp;

/**
 * Print the project usage information to a stream.
 *
 * @param stream the stream for printing
 */
- (void)printUsage:(FILE*)stream;

@end
