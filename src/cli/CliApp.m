#import "CliApp.h"

BOOL DEBUG = NO;

@implementation CliApp

- (void)application:(DDCliApplication*)app
   willParseOptions:(DDGetoptLongParser*)optionsParser {
    DDGetoptOption optionTable[] = {
        {@"input",          'i',    DDGetoptRequiredArgument},
        {@"output",         'o',    DDGetoptRequiredArgument},
        {@"timestep",       't',    DDGetoptRequiredArgument},
        {@"plot",           'p',    DDGetoptNoArgument},
        {@"no_warnings",     0 ,    DDGetoptNoArgument},
        {@"debug",           0 ,    DDGetoptNoArgument},
        {@"test",            0 ,    DDGetoptNoArgument},
        {@"help",           'h',    DDGetoptNoArgument},
        {nil,                0 ,    0}
    };
    [optionsParser addOptionsFromTable:optionTable];
}

- (int)application:(DDCliApplication*)app runWithArguments:(NSArray*)args {
    if (_debug) {
        DEBUG = YES;
    }
    if (_help) {
        [self printHelp];
        return EXIT_SUCCESS;
    }
    if (_test) {
        return [self runTests];
    }
    if (_input == nil) {
        if ([args count] == 0) {
            [self printHelp];
            return EX_USAGE;
        }
        ddfprintf(stderr, @"%@: Please specify input file\n", DDCliApp);
        [self printUsage:stderr];
        ddfprintf(stderr, @"Try '%@ --help' for more information\n", DDCliApp);
        return EX_USAGE;
    }
    if (_timestep != nil) {
        if ([_timestep floatValue] <= FLT_EPSILON) {
            NSArray* argv = [[NSProcessInfo processInfo] arguments];
            ddfprintf(stderr,
                @"%@: Option %@ requires a numeric non-zero argument\n",
                  DDCliApp, [argv containsObject:@"-t"]?@"-t":@"--timestep");
            ddfprintf(stderr, @"Try '%@ --help' for more information\n",
                      DDCliApp);
            return EX_USAGE;
        }
    }
    if (_output == nil) {
        _output =
            [[_input stringByReplacingExtensionWithString:@"csv"] retain];
    }
    else {
        // if the path is a directory, append input filename
        if ([_output hasSuffix:@"/"]) {
            NSString* filename = [_input lastPathComponent];
            NSString* newpath = [_output stringByAppendingString:filename];
            ddfprintf(stderr, @"%@: output path %@ is a directory - writing to"
                               " %@ instead\n", DDCliApp, _output, newpath);
            [_output release];
            _output = [newpath retain];
        }
    }
    if ([args count] != 0) {
        ddfprintf(stderr, @"%@: Unused arguments: %@\n", DDCliApp, args);
    }
    return [self runSimulator];
}

- (int)runSimulator {
    ParseError* error = nil;
    NSMutableArray* warnings = nil;
    SimulationScript* config =
        [SimulationScript simulationScriptFromFile:_input
                                             error:&error
                                          warnings:&warnings];
    if (config == nil) {
        ddfprintf(stderr, @"%@: Error %@\n", DDCliApp, [error errorMessage]);
        return EXIT_FAILURE;
    }
    if (warnings != nil && !_no_warnings) {
        for (ParseError* warning in warnings) {
            ddfprintf(stderr, @"%@: Warning %@\n",
                      DDCliApp, [warning errorMessage]);
        }
    }
    Gillespie* gillespie =
        [Gillespie gillespieWithState:[config initialcounts]
                         withStoptime:[config stoptime]
                        withReactions:[config reactions]];
    [gillespie runSimulation];
    [gillespie writeLogToPath:_output timestep:_timestep];
    if (_plot) {
        NSString* pngpath =
            [_output stringByReplacingExtensionWithString:@"png"];
        [gillespie plotLogToPath:pngpath timestep:_timestep];
    }
    return EXIT_SUCCESS;
}

- (int)runTests {
    BOOL allpassed = YES;
    for (Class testableclass in [TestableObject registeredSubclasses]) {
        allpassed = allpassed && [testableclass unitTest];
    }
    return allpassed ? EXIT_SUCCESS : EXIT_FAILURE;
}

- (void)printUsage:(FILE*)stream {
    ddfprintf(stream, @"%@: Usage [OPTIONS] -i SIMULATION_CONFIG_FILE\n",
              DDCliApp);
}

- (void)printHelp {
    [self printUsage:stdout];
    fprintf(stdout,
        "\n"
        "    -i, --input IN     Read simulation details from IN\n"
        "    -o, --output OUT   Write output to OUT (default: IN.csv)\n"
        "    -p, --plot         Plot outputs to OUT.png\n"
        "    -t, --timestep T   Log simulation-state every T seconds\n"
        "        --no_warnings  Suppress all warnings\n"
        "        --debug        Display debugging information\n"
        "        --test         Run unit-tests for project and exit\n"
        "    -h, --help         Show this help and exit\n"
        "\n"
        "A stochastic simulator implementing the Gillespie algorithm.\n"
        );
}

- (id)init {
    self = [super init];
    if (self == nil) {
        return nil;
    }
    return self;
}

@end
