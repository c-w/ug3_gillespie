#import <ddcli/DDCommandLineInterface.h>
#import "CliApp.h"

int main(const int argc, const char** argv) {
    return DDCliAppRunWithClass([CliApp class]);
}
