#import <AppKit/AppKit.h>
#import "gui_utilities/GuiUtilitiesHeaders.h"
#import "../shared/simulator/SimulatorHeaders.h"

/**
 * A class offering a graphical user interface to the simulator.
 *
 * @bug update buttons don't resize with windows
 */
@interface GuiApp : NSObject {
    SimulationScript* _config;
    Gillespie* _gillespie;
    NSImage* _plotcache;
}

/**
 * Sets up and populates the main menu of the application.
 */
- (void)createMenu;

/**
 * Method that is invoked from 'load configuration file' menu item.
 */
- (void)loadConfigFile:(id)sender;

/**
 * Method that is invoked from 'view configuration file' menu item.
 */
- (void)viewConfigFile:(id)sender;

/**
 * Method that is invoked from 'view simulation results' menu item.
 */
- (void)runSimulation:(id)sender;

/**
 * Method that is invoked from 'save simulation results' menu item.
 */
- (void)saveCSV:(id)sender;

/**
 * Method that is invoked from 'plot simulation results' menu item.
 * Stores the plotted graph in an instance variable so that it can later be
 * written to disk without having to re-plot the results first.
 */
- (void)plotSimulation:(id)sender;

/**
 * Method that is invoked from 'save simulation plot' menu item.
 */
- (void)savePlot:(id)sender;

/**
 * Method that is invoked from 'view help' menu item.
 */
- (void)viewHelp:(id)sender;

/**
 * Method that is invoked from 'view about' menu item.
 */
- (void)viewAbout:(id)sender;

/**
 * Creates application menu and main window.
 */
- (void)applicationWillFinishLaunching:(NSNotification*)notification;

/**
 * Sets focus to application main window.
 */
- (void)applicationDidFinishLaunching:(NSNotification*)notification;

@end

/**
 * Name of the icon to be used as application-icon.
 * A file with this name should be placed in the application bundle.
 */
NSString* APP_ICON_NAME;

/**
 * The string displayed in the simulation-results window while the simulation
 * is running.
 *
 * @bug the string gets truncated the first time that the simulation-results
 * window is created. quick+dirty fix: add some whitespace to the end
 * could be fixed better by dispatching the computation into a thread
 */
NSString* MESSAGE_RUNNING;

/**
 * The string to be displayed as title of the plot-window.
 */
NSString* WINDOW_TITLE_PLOT;

/**
 * The string to be displayed as title of the simulation-results window.
 */
NSString* WINDOW_TITLE_RUN;

/**
 * The alpha value of windows that display out-of-date content.
 */
float WINDOW_OUTOFDATE_ALPHA;

/**
 * The aspect-ratio all windows should have.
 */
float WINDOW_ASPECTRATIO;

/**
 * The title of the expanding-menu for configuration-file-related options.
 */
NSString* SUBMENU_TITLE_CONFIG;

/**
 * The title of the exanding-menu for saving-related options.
 */
NSString* SUBMENU_TITLE_SAVE;

/**
 * The title of the exanding-menu for simulation-related options.
 */
NSString* SUBMENU_TITLE_SIMULATION;

/**
 * The string displayed on the 'quit application' menu item.
 */
NSString* MENUITEM_TITLE_QUIT;

/**
 * The shortcut for the 'quit application' menu item.
 */
NSString* MENUITEM_KEY_QUIT;

/**
 * The string displayed on the 'load configuration-file' menu item.
 */
NSString* MENUITEM_TITLE_LOAD;

/**
 * The shortcut for the 'load configuration-file' menu item.
 */
NSString* MENUITEM_KEY_LOAD;

/**
 * The string displayed on the 'view configuration-file' menu item.
 */
NSString* MENUITEM_TITLE_VIEW;

/**
 * The shortcut for the 'view configuration-file' menu item.
 */
NSString* MENUITEM_KEY_VIEW;

/**
 * The string displayed on the 'view simulation results' menu item.
 */
NSString* MENUITEM_TITLE_RUN;

/**
 * The shortcut for the 'view simulation results' menu item.
 */
NSString* MENUITEM_KEY_RUN;

/**
 * The string displayed on the 'plot simulation results' menu item.
 */
NSString* MENUITEM_TITLE_PLOT;

/**
 * The shortcut for the 'plot simulation results' menu item.
 */
NSString* MENUITEM_KEY_PLOT;

/**
 * The string displayed on the 'view help' menu item.
 */
NSString* MENUITEM_TITLE_HELP;

/**
 * The shortcut for the 'view help' menu item.
 */
NSString* MENUITEM_KEY_HELP;

/**
 * The string displayed on the 'view about' menu item.
 */
NSString* MENUITEM_TITLE_ABOUT;

/**
 * The shortcut for the 'view about' menu item.
 */
NSString* MENUITEM_KEY_ABOUT;

/**
 * The string displayed on the 'save csv' menu item.
 */
NSString* MENUITEM_TITLE_SAVERUN;

/**
 * The shortcut for the 'save csv' menu item.
 */
NSString* MENUITEM_KEY_SAVERUN;

/**
 * The string displayed on the 'save plot' menu item.
 */
NSString* MENUITEM_TITLE_SAVEPLOT;

/**
 * The shortcut for the 'save plot' menu item.
 */
NSString* MENUITEM_KEY_SAVEPLOT;

/**
 * GUI-related convenience functions on Gillespie.
 *
 * @category Gillespie(GUIUtils)
 */
@interface Gillespie(GUIUtils)

/**
 * Plots the log of the invoking Gillespie and returns it as an image object.
 *
 * @param timestep the intervals at which to observe the simulation
 * @returns a plot of the log
 */
- (NSImage*)logAsImage:(NSNumber*)timestep;

@end

/**
 * GUI-related convenience functions on ParseError.
 *
 * @category ParseError(GUIUtils)
 */
@interface ParseError(GUIUtils)

/**
 * Creates an autoreleased NSAlert from the invoking ParseError.
 *
 * @returns the alert in informational style
 */
- (NSAlert*)toWarningAlert;

/**
 * Creates an autoreleased NSAlert from the invoking ParseError.
 *
 * @returns the alert in critical style
 */
- (NSAlert*)toCriticalAlert;

@end
