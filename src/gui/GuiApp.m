#import "GuiApp.h"

NSString* APP_ICON_NAME = @"Icon_48.jpeg";

NSString* MESSAGE_WELCOME =
    @"A simulator implementing the Gillespie algorithm";
NSString* MESSAGE_RUNNING =
    @"Simulation in progress... (this might take a while)"
     "                                                   ";
NSString* WINDOW_TITLE_PLOT = @"Plot of simulation";
NSString* WINDOW_TITLE_RUN = @"Simulation results";

float WINDOW_ASPECTRATIO = 4.0f/3.0f;
float WINDOW_OUTOFDATE_ALPHA = 0.85f;

NSString* SUBMENU_TITLE_CONFIG = @"Configuration...";
NSString* SUBMENU_TITLE_SIMULATION = @"Simulation...";
NSString* SUBMENU_TITLE_SAVE = @"Save...";

NSString* MENUITEM_TITLE_QUIT = @"Quit";
NSString* MENUITEM_KEY_QUIT = @"q";
NSString* MENUITEM_TITLE_LOAD = @"Load";
NSString* MENUITEM_KEY_LOAD = @"l";
NSString* MENUITEM_TITLE_VIEW = @"View";
NSString* MENUITEM_KEY_VIEW = @"v";
NSString* MENUITEM_TITLE_RUN = @"Run!";
NSString* MENUITEM_KEY_RUN = @"r";
NSString* MENUITEM_TITLE_PLOT = @"Visualise!";
NSString* MENUITEM_KEY_PLOT = @"p";
NSString* MENUITEM_TITLE_SAVERUN = @"Save CSV";
NSString* MENUITEM_KEY_SAVERUN = nil;
NSString* MENUITEM_TITLE_SAVEPLOT = @"Save Plot";
NSString* MENUITEM_KEY_SAVEPLOT = nil;
NSString* MENUITEM_TITLE_HELP = @"Help";
NSString* MENUITEM_KEY_HELP = nil;
NSString* MENUITEM_TITLE_ABOUT = @"About";
NSString* MENUITEM_KEY_ABOUT = nil;

@implementation Gillespie(GUIUtils)

- (NSImage*)logAsImage:(NSNumber*)timestep {
    NSDate* date = [NSDate date];
    NSDateFormatter* dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd_MM_yy_HH_mm_ss"];
    NSString* datestring = [dateformatter stringFromDate:date];
    [dateformatter release];
    NSFileManager* filemanager = [[NSFileManager alloc] init];
    NSString* curpath = [filemanager currentDirectoryPath];
    [filemanager release];
    NSString* tmpfilepath =
        [NSString stringWithFormat:
            @"%@/.__tmp_Gillespie_logAsImage_%@_%f.jpg",
                curpath, datestring, random_double(0, 42)];
    [self plotLogToPathAsJpg:tmpfilepath timestep:nil]; 
    NSImage* plotimage = [[NSImage alloc] initWithContentsOfFile:tmpfilepath];
    remove([tmpfilepath UTF8String]);
    return [plotimage autorelease];
}

@end

@implementation ParseError(GUIUtils)

- (NSAlert*)toWarningAlert {
    NSString* line =
        [self line] ?
            [NSString stringWithFormat:@"Line %d: \"%@\"\n",
                [self lineno], [self line]]
            : @"";
    NSString* alertinfo =
        [NSString stringWithFormat:@"File: %@\n%@Reason: %@",
            [self file], line, [self info]];
    NSAlert* alert =
        [NSAlert alertWithMessageText:@"Warning while parsing file"
                        defaultButton:@"Continue"
                      alternateButton:nil
                          otherButton:nil
        informativeTextWithFormat:alertinfo];
    [alert setAlertStyle:NSInformationalAlertStyle];
    return alert;
}

- (NSAlert*)toCriticalAlert {
    NSString* line =
        [self line] ?
            [NSString stringWithFormat:@"Line %d: \"%@\"\n",
                [self lineno], [self line]]
            : @"";
    NSString* alertinfo =
        [NSString stringWithFormat:@"File: %@\n%@Reason: %@",
            [self file], line, [self info]];
    NSAlert* alert =
        [NSAlert alertWithMessageText:@"Error while parsing file"
                        defaultButton:@"Abort"
                      alternateButton:nil
                          otherButton:nil
        informativeTextWithFormat:alertinfo];
    [alert setAlertStyle:NSCriticalAlertStyle];
    return alert;
}

@end

@interface GuiApp(PrivateMethods)

+ (NSRect)bigCenteredRect;
+ (NSRect)mediumCenteredRect;
+ (NSRect)smallCenteredRect;

@end

@implementation GuiApp(PrivateMethods)

+ (NSRect)bigCenteredRect {
    NSInteger maxwidth = [[NSScreen mainScreen] frame].size.width;
    NSInteger maxheight = [[NSScreen mainScreen] frame].size.height;
    NSInteger winwidth = (NSInteger)(maxwidth / WINDOW_ASPECTRATIO);
    NSInteger winheight = (NSInteger)(maxheight / WINDOW_ASPECTRATIO);
    NSInteger winx = (maxwidth - winwidth) / 2;
    NSInteger winy = (maxheight - winheight) / 2;
    return NSMakeRect(winx, winy, winwidth, winheight);
}

+ (NSRect)mediumCenteredRect {
    NSInteger maxwidth = [[NSScreen mainScreen] frame].size.width;
    NSInteger maxheight = [[NSScreen mainScreen] frame].size.height;
    NSInteger winwidth = (NSInteger)(maxwidth / WINDOW_ASPECTRATIO) * 2 / 3;
    NSInteger winheight = (NSInteger)(maxheight / WINDOW_ASPECTRATIO) * 2 / 3;
    NSInteger winx = (maxwidth - winwidth) / 2;
    NSInteger winy = (maxheight - winheight) / 2;
    return NSMakeRect(winx, winy, winwidth, winheight);
}

+ (NSRect)smallCenteredRect {
    NSInteger maxwidth = [[NSScreen mainScreen] frame].size.width;
    NSInteger maxheight = [[NSScreen mainScreen] frame].size.height;
    NSInteger winwidth = (NSInteger)(maxwidth / WINDOW_ASPECTRATIO) / 2;
    NSInteger winheight = (NSInteger)(maxheight / WINDOW_ASPECTRATIO) / 2;
    NSInteger winx = (maxwidth - winwidth) / 2;
    NSInteger winy = (maxheight - winheight) / 2;
    return NSMakeRect(winx, winy, winwidth, winheight);
}

+ (NSRect)plotsizeCenteredRect {
    NSInteger maxwidth = [[NSScreen mainScreen] frame].size.width;
    NSInteger maxheight = [[NSScreen mainScreen] frame].size.height;
    NSInteger winwidth = 640;
    NSInteger winheight = 480;
    NSInteger winx = (maxwidth - winwidth) / 2;
    NSInteger winy = (maxheight - winheight) / 2;
    return NSMakeRect(winx, winy, winwidth, winheight);
}

@end

@implementation GuiApp

- (void)loadConfigFile:(id)sender {
    // create open panel
    NSOpenPanel* openpanel = [NSOpenPanel openPanel];
    [openpanel setTitle:@"Select configuration file to load..."];
    [openpanel setCanChooseFiles:YES];
    [openpanel setCanChooseDirectories:NO];
    [openpanel setAllowsMultipleSelection:NO];
    NSInteger clicked = [openpanel runModal];
    if (clicked == NSFileHandlingPanelOKButton) {
        // validate file selected in panel
        for (NSString* filename in [openpanel filenames]) {
            ParseError* error = nil;
            NSMutableArray* warnings = nil;
            SimulationScript* loadedconfig =
                [SimulationScript simulationScriptFromFile:filename
                                                     error:&error
                                                  warnings:&warnings];
            // error aborts loading
            if (loadedconfig == nil) {
                NSAlert* alert = [error toCriticalAlert];
                [alert runModal];
                return;
            }
            // display warnings, one at a time
            if (warnings != nil) {
                for (ParseError* warning in warnings) {
                    NSAlert* alert = [warning toWarningAlert];
                    [alert runModal];
                }
            }
            // do nothing if same file is loaded
            if ([loadedconfig isEqual:_config]) {
                return;
            }
            for (NSWindow* oldwin in [NSApp windows]) {
                // if a file-view window is open, update it
                if ([[oldwin title] isEqualToString:[_config filepath]]) {
                    NSString* newcontent = [loadedconfig filecontents];
                    NSString* newtitle =
                        [loadedconfig filepath];
                    [oldwin setTitle:newtitle];
                    [oldwin updateWith:newcontent];
                }
                // and add update button to all other windows
                else if ([[oldwin title] isEqualToString:WINDOW_TITLE_PLOT]) {
                    [oldwin removeUpdateButtons];
                    [oldwin setAlphaValue:WINDOW_OUTOFDATE_ALPHA];
                }
                else if ([[oldwin title] isEqualToString:WINDOW_TITLE_RUN]) {
                    [oldwin addUpdateButton:@selector(runSimulation:)];
                    [oldwin setAlphaValue:WINDOW_OUTOFDATE_ALPHA];
                }
            }
            _config = [loadedconfig retain];
            // can now view and run a simulation - enable menu items
            [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_RUN]
                setAction:@selector(runSimulation:)];
            [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_VIEW]
                setAction:@selector(viewConfigFile:)];
            // can no longer plot a simulation or save one - disable menu items
            [[[NSApp mainMenu] getItemWithTitleRecursive:
                MENUITEM_TITLE_SAVERUN] setAction:NULL];
            [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_PLOT]
                setAction:NULL];
            [[[NSApp mainMenu] getItemWithTitleRecursive:
                MENUITEM_TITLE_SAVEPLOT] setAction:NULL];
        }
    }
}

- (void)viewConfigFile:(id)sender {
    NSString* viewwindowtitle = [_config filepath];
    // if there already is a config-file window, display it
    NSWindow* viewwindow = [NSApp findWindowWithTitle:viewwindowtitle];
    if (viewwindow != nil) {
        [viewwindow makeKeyAndOrderFront:nil];
        return;
    }
    // otherwise create a new one
    NSRect rect = [GuiApp mediumCenteredRect];
    NSUInteger style =
        NSTitledWindowMask | NSClosableWindowMask | NSResizableWindowMask |
        NSMiniaturizableWindowMask;
    NSWindow* window =
        [[NSWindow windowWithTextViewContainingString:[_config filecontents]
                                             withRect:rect
                                            withStyle:style]
        retain];
    [window setTitle:viewwindowtitle];
}

- (void)runSimulation:(id)sender {
    NSWindow* simulationwindow = [NSApp findWindowWithTitle:WINDOW_TITLE_RUN];
    // if update button was clicked to get us here, delete it
    if ([sender isKindOfClass:[NSButton class]]) {
        [sender removeFromSuperview];
        [simulationwindow setAlphaValue:1.0f];
    }
    // if a plot window exists and doesn't have a refresh button, put one in
    NSWindow* plotwindow = [NSApp findWindowWithTitle:WINDOW_TITLE_PLOT];
    if (plotwindow != nil) {
        BOOL addbutton = YES;
        for (NSView* subview in [[plotwindow contentView] subviews]) {
            if ([subview isKindOfClass:[NSButton class]]) {
                addbutton = NO;
                break;
            }
        }
        if (addbutton) {
            [plotwindow addUpdateButton:@selector(plotSimulation:)];
            [plotwindow setAlphaValue:WINDOW_OUTOFDATE_ALPHA];
        }
    }
    // if a simulation window exists, bring it to the front
    // also remove any update buttons that might still be around
    if (simulationwindow != nil) {
        [simulationwindow updateWith:MESSAGE_RUNNING];
        [simulationwindow removeUpdateButtons];
    }
    else {
        // otherwise create a new simulation window
        NSRect rect = [GuiApp smallCenteredRect];
        NSUInteger style =
            NSTitledWindowMask | NSClosableWindowMask | NSResizableWindowMask |
            NSMiniaturizableWindowMask;
        simulationwindow =
            [[NSWindow windowWithTextViewContainingString:MESSAGE_RUNNING
                                                 withRect:rect
                                                withStyle:style]
            retain];
        [simulationwindow setTitle:WINDOW_TITLE_RUN];
        // make text field monospace (size=0.0 sets size to user default)
        [[[simulationwindow contentView] documentView]
            setFont:[NSFont userFixedPitchFontOfSize:0.0f]];
    }
    [simulationwindow makeKeyAndOrderFront:nil];
    // enable menu items: plot, save csv
    [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_PLOT]
        setAction:@selector(plotSimulation:)];
    [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_SAVERUN]
        setAction:@selector(saveCSV:)];
    // disable menu item: save plots
    [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_SAVEPLOT]
        setAction:NULL];
    // get rid of any previously cached plots
    [_plotcache release];
    _plotcache = nil;
    // run simulation
    _gillespie =
        [[Gillespie gillespieWithState:[_config initialcounts]
                         withStoptime:[_config stoptime]
                        withReactions:[_config reactions]]
        retain];
    NSDate* start = [NSDate date];
    [_gillespie runSimulation];
    double timeelapsedseconds = [start timeIntervalSinceNow] * -1000.0f;
    NSString* csvstring = [_gillespie logAsPrettyCSV];
    [simulationwindow updateWith:csvstring];
    // let the user know that the simulation is done
    NSString* alerttext =
        [NSString stringWithFormat:@"The simulation took %f seconds to run.",
            timeelapsedseconds];
    NSAlert* alert =
        [NSAlert alertWithMessageText:@"Simulation finished"
                        defaultButton:@"Continue"
                      alternateButton:nil
                          otherButton:nil
            informativeTextWithFormat:alerttext];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [simulationwindow makeKeyAndOrderFront:nil];
    [alert runModal];
}

- (void)plotSimulation:(id)sender {
    NSWindow* plotwindow = [NSApp findWindowWithTitle:WINDOW_TITLE_PLOT];
    // if update button was clicked to get us here, delete it
    if ([sender isKindOfClass:[NSButton class]]) {
        [sender removeFromSuperview];
        [plotwindow setAlphaValue:1.0f];
    }
    // generate a new plot if we don't have one cached
    if (_plotcache == nil) {
        _plotcache = [[_gillespie logAsImage:nil] retain];
    }
    // remove any update buttons that might still be around
    if (plotwindow != nil) {
        [plotwindow removeUpdateButtons];
    }
    // create a new plot window if we don't have one
    else {
        NSRect rect = [GuiApp plotsizeCenteredRect];
        NSImageView* imageview = [[NSImageView alloc] initWithFrame:rect];
        NSUInteger style =
            NSTitledWindowMask | NSMiniaturizableWindowMask |
            NSClosableWindowMask | NSResizableWindowMask;
        plotwindow =
            [[[NSWindow alloc] initWithContentRect:rect
                                        styleMask:style
                                          backing:NSBackingStoreBuffered
                                            defer:NO]
            retain];
        [plotwindow setTitle:WINDOW_TITLE_PLOT];
        [plotwindow setContentView:imageview];
        [imageview release];
    }
    // enable menu item: save plots
    [[[NSApp mainMenu] getItemWithTitleRecursive:MENUITEM_TITLE_SAVEPLOT]
        setAction:@selector(savePlot:)];
    [[plotwindow contentView] setImage:_plotcache];
    [plotwindow makeKeyAndOrderFront:nil];
}

- (void)saveCSV:(id)sender {
    NSSavePanel* savepanel = [NSSavePanel savePanel];
    [savepanel setTitle:@"Save simulation results..."];
    NSInteger clicked = [savepanel runModal];
    if (clicked == NSOKButton) {
        [_gillespie writeLogToPath:[savepanel filename] timestep:nil];
    }
}

- (void)savePlot:(id)sender {
    NSSavePanel* savepanel = [NSSavePanel savePanel];
    [savepanel setRequiredFileType:@"jpeg"];
    [savepanel setTitle:@"Save plot..."];
    NSInteger clicked = [savepanel runModal];
    if (clicked == NSOKButton) {
        [_plotcache saveAsJpegWithName:[savepanel filename]];
    }
}

- (void)viewHelp:(id)sender {
    NSString* helptext =
        [NSString stringWithFormat:
            @"Loading a simulation from a configuration file: '%@' >> '%@'\n"
             "Running a simulation: '%@' >> '%@' (results shown in table)\n"
             "Plotting the results of a simulation: '%@' >> '%@'\n"
             "\n"
             "For more information on the syntax of configuration-files "
             "or on how the simulator works, "
             "please refer to the full specification at http://goo.gl/wtXQO",
                SUBMENU_TITLE_CONFIG, MENUITEM_TITLE_LOAD,
                SUBMENU_TITLE_SIMULATION, MENUITEM_TITLE_RUN,
                SUBMENU_TITLE_SIMULATION, MENUITEM_TITLE_PLOT];
    NSAlert* alert =
        [NSAlert alertWithMessageText:@"Help"
                        defaultButton:@"Ok"
                      alternateButton:nil
                          otherButton:nil
            informativeTextWithFormat:helptext];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert runModal];
}

- (void)viewAbout:(id)sender {
    NSAlert* alert =
        [NSAlert alertWithMessageText:@"About"
                        defaultButton:@"Ok"
                      alternateButton:nil
                          otherButton:nil
        informativeTextWithFormat:
            @"A stochastic discrete-event simulator for chemical reactions.\n"
             "\n"
             "Written by c-w for the UG3-course CSLP (2012/2013).\n"
             "Developed on XUbuntu 12.04 amd64.\n"
             "Tested on Ubuntu 12.04 amd64/i386 and Ubuntu 12.10 amd64/i386.\n"
             "\n"
             "Find the full specification at http://goo.gl/wtXQO"];
    [alert setAlertStyle:NSInformationalAlertStyle];
    [alert runModal];
}

- (void)createMenu {
    NSMenuItem* quit =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_QUIT
                                    action:@selector(terminate:)
                             keyEquivalent:MENUITEM_KEY_QUIT]
        autorelease];
    NSMenuItem* configtree = [[[NSMenuItem alloc] init] autorelease];
    [configtree setTitle:SUBMENU_TITLE_CONFIG];
    NSMenuItem* load =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_LOAD
                                    action:@selector(loadConfigFile:)
                             keyEquivalent:MENUITEM_KEY_LOAD]
        autorelease];
    NSMenuItem* view =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_VIEW
                                    action:NULL
                             keyEquivalent:MENUITEM_KEY_VIEW]
        autorelease];
    NSMenuItem* simulationtree =
        [[[NSMenuItem alloc] init] autorelease];
    [simulationtree setTitle:SUBMENU_TITLE_SIMULATION];
    NSMenuItem* run =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_RUN
                                    action:NULL
                             keyEquivalent:MENUITEM_KEY_RUN]
        autorelease];
    NSMenuItem* savetree =
        [[[NSMenuItem alloc] init] autorelease];
    [savetree setTitle:SUBMENU_TITLE_SAVE];
    NSMenuItem* saverun =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_SAVERUN
                                    action:NULL
                             keyEquivalent:MENUITEM_KEY_SAVERUN]
        autorelease];
    NSMenuItem* plot =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_PLOT
                                    action:NULL
                             keyEquivalent:MENUITEM_KEY_PLOT]
        autorelease];
    NSMenuItem* saveplot =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_SAVEPLOT
                                    action:NULL
                             keyEquivalent:MENUITEM_KEY_SAVEPLOT]
        autorelease];
    NSMenuItem* help =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_HELP
                                    action:@selector(viewHelp:)
                             keyEquivalent:MENUITEM_KEY_HELP]
        autorelease];
    NSMenuItem* about =
        [[[NSMenuItem alloc] initWithTitle:MENUITEM_TITLE_ABOUT
                                    action:@selector(viewAbout:)
                             keyEquivalent:MENUITEM_KEY_ABOUT]
        autorelease];
    // create and populate submenus 
    NSMenu* configmenu = [[[NSMenu alloc] init] autorelease];
    [configmenu addItem:load];
    [configmenu addItem:view];
    NSMenu* simulationmenu = [[[NSMenu alloc] init] autorelease];
    [simulationmenu addItem:run];
    [simulationmenu addItem:plot];
    [simulationmenu addItem:savetree];
    NSMenu* savemenu = [[[NSMenu alloc] init] autorelease];
    [savemenu addItem:saverun];
    [savemenu addItem:saveplot];
    // add submenus to main menu items
    [configtree setSubmenu:configmenu];
    [simulationtree setSubmenu:simulationmenu];
    [savetree setSubmenu:savemenu];
    // add items to main menu
    NSMenu* mainmenu = [[[NSMenu alloc] init] autorelease];
    [mainmenu addItem:quit];
    [mainmenu addItem:configtree];
    [mainmenu addItem:simulationtree];
    [mainmenu addItem:help];
    [mainmenu addItem:about];
    // tell the app about the menu
    [NSApp setMainMenu:mainmenu];
}

- (void)applicationWillFinishLaunching:(NSNotification*)notification {
    [self createMenu];
}

- (void)applicationDidFinishLaunching:(NSNotification*)notification {
    [NSApp setApplicationIconImage:[NSImage imageNamed:APP_ICON_NAME]];
}

- (BOOL)applicationShouldTerminateAfterLastWindowClosed:(NSApplication*)app {
    return NO;
}

- (void)dealloc {
    [_config release];
    [_gillespie release];
    [_plotcache release];
    [super dealloc];
}

@end
