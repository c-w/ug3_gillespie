#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSApplication.
 *
 * @category NSApplication(Utilities)
 */
@interface NSApplication(Utilities)

/**
 * Searches the invoking application for a window with a given title and
 * returns it.
 *
 * @returns the desired window or nil if no such window was found
 */
- (NSWindow*)findWindowWithTitle:(NSString*)title;

@end
