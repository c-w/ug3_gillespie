#import "NSApplication+Utilities.h"

@implementation NSApplication(Utilities)

- (NSWindow*)findWindowWithTitle:(NSString*)title {
    for (NSWindow* window in [self windows]) {
        if ([[window title] isEqualToString:title]) {
            return window;
        }
    }
    return nil;
}

@end
