#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

/**
 * Convenience functions on NSImage.
 *
 * @category NSImage(Utilities)
 */
@interface NSImage(Utilities)

/**
 * Writes the invoking NSImage to disk.
 *
 * @param filename the path at which to save the image
 */
- (void)saveAsJpegWithName:(NSString*)filename;

@end
