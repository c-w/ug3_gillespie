#import "NSImage+Utilities.h"

@implementation NSImage(Utilities)

- (void)saveAsJpegWithName:(NSString*)filename {
    NSData* imagedata = [self TIFFRepresentation];
    NSBitmapImageRep* imagerep = [NSBitmapImageRep imageRepWithData:imagedata];
    NSDictionary* imageprops =
        [NSDictionary dictionaryWithObject:
            [NSNumber numberWithFloat:1.0f] forKey:NSImageCompressionFactor];
    imagedata =
        [imagerep representationUsingType:NSJPEGFileType
                               properties:imageprops];
    [imagedata writeToFile:filename atomically:NO];   
}

@end
