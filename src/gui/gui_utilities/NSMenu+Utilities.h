#import <Foundation/Foundation.h>
#import <AppKit/AppKit.h>

/**
 * Convenience functions on NSMenu.
 *
 * @category NSMenu(Utilities)
 */
@interface NSMenu(Utilities)

/**
 * Searches for a NSMenuItem with some title in the invoking NSMenu and in any
 * submenus thereof.
 * Returns nil if no such item can be found.
 * Serach is depth-first.
 *
 * @param title the title of the NSMenuItem to search for
 * @returns the NSMenuItem with the title or nil if it can't be found
 */
- (NSMenuItem*)getItemWithTitleRecursive:(NSString*)title;

@end
