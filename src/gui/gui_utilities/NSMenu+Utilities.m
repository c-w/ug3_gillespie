#import "NSMenu+Utilities.h"

@implementation NSMenu(Utilities)

- (NSMenuItem*)getItemWithTitleRecursive:(NSString*)title {
    for (NSMenuItem* menuitem in [self itemArray]) {
        if ([[menuitem title] isEqualToString:title]) {
            return menuitem;
        }
        if ([menuitem hasSubmenu]) {
            NSMenuItem* recursivesearchresult =
                [[menuitem submenu] getItemWithTitleRecursive:title];
            if (recursivesearchresult != nil) {
                return recursivesearchresult;
            }
        }
    }
    return nil;
}

@end
