#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSWindow.
 *
 * @category NSWindow(Utilities)
 */
@interface NSWindow(Utilities)

/**
 * Method to re-set the contents of the invoking window.
 *
 * @param newcontent the content to set
 */
- (void)updateWith:(id)newcontent;

/**
 * Create a new autoreleased scrollable window displaying some text.
 *
 * @param text the string to be displayed in the window
 * @param rect the rectangle containing the window
 * @param stylemask the window style
 */
+ (NSWindow*)windowWithTextViewContainingString:(NSString*)text
                                       withRect:(NSRect)rect
                                      withStyle:(NSUInteger)stylemask;

/**
 * Adds a NSButton displaying "update" with some target to the invoking window.
 *
 * @param action the method to be invoked on button-click
 */
- (void)addUpdateButton:(SEL)action;

/**
 * Removes all "update" buttons from the invoking window.
 */
- (void)removeUpdateButtons;

@end

/**
 * The string to be displayed as title for "update" buttons.
 */
NSString* UPDATE_BUTTON_TITLE;
