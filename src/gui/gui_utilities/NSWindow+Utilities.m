#import "NSWindow+Utilities.h"
#import <objc/runtime.h>

NSString* UPDATE_BUTTON_TITLE = @"Click to refresh";

@implementation NSWindow(Utilities)

- (void)updateWith:(id)content {
    id view = [self contentView];
    if ([view isKindOfClass:[NSScrollView class]]) {
        view = (NSScrollView*)view;
        id documentview = [view documentView];
        if ([documentview isKindOfClass:[NSTextView class]]) {
            documentview = (NSTextView*)documentview;
            [documentview setString:(NSString*)content];
            [documentview scrollRangeToVisible:NSMakeRange(0.0f, 0.0f)];
        }
    }
    else if ([view isKindOfClass:[NSImageView class]]) {
        view = (NSImageView*)view;
        [view setImage:(NSImage*)content];
    }
}

+ (NSWindow*)windowWithTextViewContainingString:(NSString*)text
                                       withRect:(NSRect)rect
                                     withStyle:(NSUInteger)stylemask {
    NSWindow* window =
        [[NSWindow alloc] initWithContentRect:rect
                                     styleMask:stylemask
                                       backing:NSBackingStoreBuffered
                                         defer:NO];
    // --> step 1: set up scroll view
    NSScrollView* scrollview =
        [[[NSScrollView alloc] initWithFrame:[window frame]] autorelease];
    NSSize contentsize = [scrollview contentSize];
    [scrollview setBorderType:NSNoBorder];
    [scrollview setHasVerticalScroller:YES];
    [scrollview setHasHorizontalScroller:YES];
    [scrollview setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
    // --> step 2: set up text view
    NSTextView* textview =
        [[[NSTextView alloc] initWithFrame:
            NSMakeRect(0, 0, contentsize.width, contentsize.height)]
        autorelease];
    [textview setMinSize:NSMakeSize(0.0f, contentsize.height)];
    [textview setMaxSize:NSMakeSize(FLT_MAX, FLT_MAX)];
    [textview setVerticallyResizable:YES];
    [textview setHorizontallyResizable:YES];
    [textview setAutoresizingMask:NSViewWidthSizable | NSViewHeightSizable];
    [[textview textContainer]
        setContainerSize:NSMakeSize(FLT_MAX, FLT_MAX)];
    [[textview textContainer] setWidthTracksTextView:NO];
    // --> step 3: assemble
    [scrollview setDocumentView:textview];
    [window setContentView:scrollview];
    [window makeKeyAndOrderFront:nil];
    [window makeFirstResponder:textview];
    // display show text (scroll to start of text)
    [textview insertText:text];
    [textview setEditable:NO];
    [textview scrollRangeToVisible:NSMakeRange(0.0f, 0.0f)];
    return [window autorelease];
}

- (void)addUpdateButton:(SEL)action {
    NSRect windowrect = [self frame];
    int buttonwidth = windowrect.size.width * 0.25;
    int buttonheight = windowrect.size.height * 0.25;
    int buttonx = (windowrect.size.width - buttonwidth) / 2;
    int buttony = (windowrect.size.height - buttonheight) / 2;
    NSRect buttonrect =
        NSMakeRect(buttonx, buttony, buttonwidth, buttonheight);
    NSButton* button =
        [[[NSButton alloc] initWithFrame:buttonrect]
        autorelease];
    [button setTitle:UPDATE_BUTTON_TITLE];
    [button setBezelStyle:NSRoundedBezelStyle];
    [button setTarget:self];
    [button setAction:action];
    [[self contentView] addSubview:button];
}

- (void)removeUpdateButtons {
    for (id subview in [[self contentView] subviews]) {
        if ([subview isKindOfClass:[NSButton class]]) {
            NSButton* button = (NSButton*)subview;
            if ([[button title] isEqualToString:UPDATE_BUTTON_TITLE]) {
                [subview removeFromSuperview];
            }
        }
    }
}

@end
