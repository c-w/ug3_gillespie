#import <AppKit/AppKit.h>
#import "GuiApp.h"

int main(const int argc, const char** argv) {
    [NSApplication sharedApplication];
    [NSApp setDelegate:[GuiApp new]];
    return NSApplicationMain(argc, argv);
}
