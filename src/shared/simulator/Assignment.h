#import "Constants.h"
#import "ParseError.h"

/**
 * A class representing an assignment in a SimulationScript.
 * Assignments are of the form "variable assignment_operator expression".
 * Assignments can be:
 * - value-assignments (e.g. X = 10, t = 5e2, m = 0.33).
 * - function-assignments (e.g. m : A + B -> C).
 */
@interface Assignment : TestableObject {
    NSString* _variable;
    id _expression;
    NSString* _file;
    NSString* _line;
    NSNumber* _lineno;
}

/**
 * The variable-part of the assignment i.e. 'X' in the assignment 'X := Y'.
 *
 * @returns the variable of the assignment
 */
- (NSString*)variable;

/**
 * The expression-part of the assignment i.e. 'Y' in the assignment 'X := Y'.
 * 'Y' is:
 * - a NSString for function-assignments.
 * - a NSNumber for value-assignments.
 *
 * @returns the expression of the assignment
 */
- (id)expression;

/**
 * The file in which the assignment was made.
 *
 * @returns the file containing the assignment
 */
- (NSString*)file;

/**
 * The line in which the assignment was made.
 *
 * @returns the line containing the assignment
 */
- (NSString*)line;

/**
 * The line number in which the assignment was made.
 *
 * @returns the line number of the assignment
 */
- (NSInteger)lineno;

/**
 * Sets the file in which the assignment is made.
 *
 * @param file the file containing the assignment
 */
- (void)setFile:(NSString*)file;

/**
 * Sets the line in which the assignment is made.
 *
 * @param line the line containing the assignment
 */
- (void)setLine:(NSString*)line;

/**
 * Sets the line number in which the assignment is made.
 *
 * @param lineno the line number containing the assignment
 */
- (void)setLineno:(NSInteger)lineno;

/**
 * Initialise a new Assignment object.
 *
 * @param variable the X-half of the assignment
 * @param expression the Y-half of the assignment
 * @returns a new assignment object representing X := Y
 */
- (id)initWithVariable:(NSString*)variable andExpression:(id)expression;

/**
 * Parse a string to create an Assignment representing a function assignment.
 * e.g. m : A + B -> C.
 *
 * @param string the string to parse the assignment from
 * @param error a pointer at which to store errors encountered during parsing
 * @returns the Assignment object or nil if there was an error in parsing
 */
+ (Assignment*)parseFunctionAssignmentFromString:(NSString*)string
                                       withError:(ParseError**)error;

/**
 * Parse a string to create an Assignment representing a value assignment.
 * e.g. X = 10.
 *
 * @param string the string to parse the assignment from
 * @param error a pointer at which to store errors encountered during parsing
 * @returns the Assignment object or nil if there was an error in parsing
 */
+ (Assignment*)parseValueAssignmentFromString:(NSString*)string
                                    withError:(ParseError**)error;

@end
