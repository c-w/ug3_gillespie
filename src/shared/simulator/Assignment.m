#import "Assignment.h"

@interface Assignment(PrivateMethods)

- (void)setExpression:(id)expression;
+ (Assignment*)parseAssignmentFromString:(NSString*)string
                            withOperator:(NSString*)operator
                               withError:(ParseError**)error;

@end

@implementation Assignment(PrivateMethods)

- (void)setExpression:(id)expression {
    [_expression release];
    _expression = [expression retain];
}

+ (Assignment*)parseAssignmentFromString:(NSString*)string
                            withOperator:(NSString*)operator
                               withError:(ParseError**)error {
    NSArray* tokens = [string nonEmptyComponentsSeparatedByString:operator];
    // make sure that assignment has two things on either side of operator
    if ([tokens count] < 2) {
        *error = [ParseError assignmentWithTooFewOperands];        
        return nil;
    }
    else if ([tokens count] > 2) {
       *error = [ParseError assignmentWithTooManyOperands];
        return nil;
    }
    Assignment* assignment =
        [[Assignment alloc] initWithVariable:[tokens objectAtIndex:0]
                               andExpression:[tokens objectAtIndex:1]];
    return [assignment autorelease];
}

@end

@implementation Assignment

- (NSString*)variable {
    return _variable;
}

- (id)expression {
    return _expression;
}

- (NSString*)file {
    return _file;
}

- (NSString*)line {
    return _line;
}

- (NSInteger)lineno {
    return [_lineno integerValue];
}

- (void)setFile:(NSString*)file {
    if (_file == nil) {
        _file = [file retain];
    }
}

- (void)setLine:(NSString*)line {
    if (_line == nil) {
        _line = [line retain];
    }
}

- (void)setLineno:(NSInteger)lineno {
    if (_lineno == nil) {
        _lineno = [[NSNumber numberWithInteger:lineno] retain];
    }
}

// disable default constructor
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self release];
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)dealloc {
    [_variable release];
    [_expression release];
    [_file release];
    [_line release];
    [_lineno release];
    [super dealloc];
}

- (NSString*)description {
    NSString* description =
        [NSString stringWithFormat:@"%@ := %@", _variable, _expression];
    return description;
}

- (id)initWithVariable:(NSString*)variable andExpression:(id)expression {
    self = [super init];
    if (self) {
        _variable = [variable retain];
        _expression = [expression retain];
    }
    return self;
}

+ (Assignment*)parseFunctionAssignmentFromString:(NSString*)string
                                       withError:(ParseError**)error {
    Assignment* assignment =
        [Assignment parseAssignmentFromString:string
                                 withOperator:FUNCTION_DECLARATION_OPERATOR
                                    withError:error];
    if (assignment == nil) {
        return nil;
    }
    NSString* function = [assignment expression];
    // make sure there is only a single '->' in the function
    NSArray* functiontokens =
        [function componentsSeparatedByString:FUNCTION_RESULT_OPERATOR];
    if ([functiontokens count] < 2) {
        *error = [ParseError assignmentWithTooFewOperands];        
        return nil;
    }
    else if ([functiontokens count] > 2) {
       *error = [ParseError assignmentWithTooManyOperands];
        return nil;
    }
    // make sure that function (X -> Y) only contains legal tokens:
    // both X and Y should satisfy (molecule (+ molecule)*)?
    NSString* functioninput = [functiontokens objectAtIndex:0];
    NSString* functionoutput = [functiontokens objectAtIndex:1];
    NSString* combinator = [FUNCTION_COMBINATION_OPERATOR escapeForRegex];
    NSString* combinatorregex =
        [NSString stringWithFormat:@"(%@)", combinator];
    NSString* nameregex = [NSString stringWithFormat:@"([A-Z][A-Za-z]*)"];
    NSString* isvalidregex =
        [NSString stringWithFormat:
            @"^(%@(%@%@)*)?$", nameregex, combinatorregex, nameregex];
    if (!([functioninput matchesRegex:isvalidregex] &&
          [functionoutput matchesRegex:isvalidregex])) {
        *error = [ParseError invalidTokenInFunctionDeclaration];
        return nil;
    }
    return assignment;
}

+ (Assignment*)parseValueAssignmentFromString:(NSString*)string
                                    withError:(ParseError**)error {
    Assignment* assignment =
        [Assignment parseAssignmentFromString:string
                                 withOperator:VALUE_ASSIGNMENT_OPERATOR
                                   withError:error];
    if (assignment == nil) {
        return nil;
    }
    NSString* valstr = [assignment expression];
    // make sure that valuestr is numeric
    if (![valstr isNumeric]) {
        *error = [ParseError assignmentToNonNumeric];
        return nil;
    }
    // make sure that valstr represents something positive
    if ([valstr hasPrefix:@"-"]) {
        *error = [ParseError assignmentToNegative];
        return nil;
    }
    NSNumber* value = [NSNumber numberWithString:valstr];
    // make sure the assignment fits into a double
    double doublevalue = [value doubleValue];
    if (doublevalue < 0 || doublevalue >= DBL_MAX) {
        *error = [ParseError assignmentOverflow];
        return nil;
    }
    [assignment setExpression:value];
    return assignment;
}

+ (void)load {
    [Assignment registerSubclass:self];
}

+ (BOOL)unitTest {
    print(@"Running unit-tests for %s", __FILE__);
    ParseError* error;
    BOOL rvalok, errorok;
    // missing assign-to value should give error
    error = nil;
    Assignment* toofewoperandsassignment =
        [Assignment parseValueAssignmentFromString:@"f=" withError:&error];
    rvalok = toofewoperandsassignment == nil;
    errorok = [error code] == ParseErrorCode_AssignmentWithTooFewOperands;
    if (!(rvalok && errorok)) {
        print(@"--> problem with assignments with too few operands: should "
               "give error with code %d but is %d",
               ParseErrorCode_AssignmentWithTooFewOperands, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // assignment to multiple values should give error
    error = nil;
    Assignment* toomanyoperandsassignment =
        [Assignment parseValueAssignmentFromString:@"E=5=10" withError:&error];
    rvalok = toomanyoperandsassignment == nil;
    errorok = [error code] == ParseErrorCode_AssignmentWithTooManyOperands;
    if (!(rvalok && errorok)) {
        print(@"--> problem with assignments with too many operands: should "
               "give error with code %d but is %d",
               ParseErrorCode_AssignmentWithTooManyOperands, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // assignment to non-numeric values should give error
    error = nil;
    Assignment* nonnumericassignment =
        [Assignment parseValueAssignmentFromString:@"t=foo" withError:&error];
    rvalok = nonnumericassignment == nil;
    errorok = [error code] == ParseErrorCode_AssignmentToNonNumeric;
    if (!(rvalok && errorok)) {
        print(@"--> problem with assignments to non-numeric values: should "
               "give error with code %d but is %d",
               ParseErrorCode_AssignmentToNonNumeric, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // assignment to negative values should give error
    error = nil;
    Assignment* negativeassignment =
        [Assignment parseValueAssignmentFromString:@"t=-5" withError:&error];
    rvalok = negativeassignment == nil;
    errorok = [error code] == ParseErrorCode_AssignmentToNegative;
    if (!(rvalok && errorok)) {
        print(@"--> problem with assignments to negative values: should give "
               "error with code %d but is %d",
               ParseErrorCode_AssignmentToNegative, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // assignment to too big/small values should give error
    error = nil;
    Assignment* overflowassignment =
        [Assignment parseValueAssignmentFromString:@"A=1e500"
                                         withError:&error];
    rvalok = overflowassignment == nil;
    errorok = [error code] == ParseErrorCode_AssignmentOverflow;
    if (!(rvalok && errorok)) {
        print(@"--> problem with assignments to overflow values: should give "
               "error with code %d but is %d",
               ParseErrorCode_AssignmentOverflow, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // functions with unknown operands should give error
    error = nil;
    Assignment* illegaltokenassignment =
        [Assignment parseFunctionAssignmentFromString:@"d:X+A->Z-Z"
                                            withError:&error];
    rvalok = illegaltokenassignment == nil;
    errorok = [error code] == ParseErrorCode_InvalidTokenInFunctionDeclaration;
    if (!(rvalok && errorok)) {
        print(@"--> problem with functions with illegal tokens: should give "
               "error with code %d but is %d",
               ParseErrorCode_InvalidTokenInFunctionDeclaration, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // functions with too many operands should give error
    error = nil;
    Assignment* manyplusassignment =
        [Assignment parseFunctionAssignmentFromString:@"d:X+->Z"
                                            withError:&error];
    rvalok = manyplusassignment == nil;
    errorok = [error code] == ParseErrorCode_InvalidTokenInFunctionDeclaration;
    if (!(rvalok && errorok)) {
        print(@"--> problem with functions with illegal tokens: should give "
               "error with code %d but is %d",
               ParseErrorCode_InvalidTokenInFunctionDeclaration, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    return YES;
}

@end
