/**
 * The inline-comment delimiters in simulation scripts
 * Default: '#'
 */
extern NSString* const COMMENT_START;

/**
 * The value-assignment operator in simulation scripts
 * Default: '='
 */
extern NSString* const VALUE_ASSIGNMENT_OPERATOR;

/**
 * The function/reaction declaration operator in simulation scripts
 * Default: ':'
 */
extern NSString* const FUNCTION_DECLARATION_OPERATOR;

/**
 * The function/reaction-operand combination operator in simulation scripts
 * Default: '+'
 */
extern NSString* const FUNCTION_COMBINATION_OPERATOR;

/**
 * The function/reaction-result operator in simulation scripts
 * Default: '->'
 */
extern NSString* const FUNCTION_RESULT_OPERATOR;
