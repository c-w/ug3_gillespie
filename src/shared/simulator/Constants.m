#import "Constants.h"

NSString* const COMMENT_START = @"#";
NSString* const VALUE_ASSIGNMENT_OPERATOR = @"=";
NSString* const FUNCTION_DECLARATION_OPERATOR = @":";
NSString* const FUNCTION_COMBINATION_OPERATOR = @"+";
NSString* const FUNCTION_RESULT_OPERATOR = @"->";
