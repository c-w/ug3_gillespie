#import <gnuplot_i/gnuplot_i.h>
#import "Reaction.h"

/**
 * Struct representing a linked list of time/state events.
 */
typedef struct Node {
    double time;
    long* state;
    struct Node* next;
} Node;

/**
 * A class implementing the Gillespie stochastic simulation algorithm
 *
 * The algorithm:
 * - Setup.
 *      - there are N types of molecules S_1,...,S_N.
 *      - X_i(t) is the number of molecules of type S_i at time t.
 *      - x is the molecule-population-vector (X_1(t),...,X_N(t)).
 *      - we are given x_0.
 *      - there are M reactions R_1,...,R_M.
 *      - each reaction R_j has a state-change vector v_j.
 *      - v_j describes how firing the reaction changes the system.
 *      - each reaction channel R_j has a propensity function a_j(x).
 *      - a_j(x) represents how likely R_j is to happen in simulation-state x.
 * - Simulation up to a point T.
 *      - let t = 0.
 *      - while (t < T) do:
 *           - let r_1 and r_2 be random uniform numbers in (0,1).
 *           - let a_0 = (sum from k=1 to M of a_k(x)).
 *           - let tau = 1/a_0 * ln(1/r_1).
 *           - let j be the smallest integer satisfying
 *                 (sum from k=1 to j of a_k(x)) > r_2 * a_0.
 *           - let x = x + v_j.
 *           - let t = t + tau.
 *      - done.
 * @see http://www.slideshare.net/sgilmore/the-stochastic-simulation-algorithm
 */
@interface Gillespie : NSObject {
    long* _state;
    size_t _len;
    double _stoptime;
    NSArray* _reactions;
    NSDictionary* _log;
    Node* _linkedlog;
}

/**
 * Returns a dictionary of state-changes in the reaction.
 * Each key corresponds to a time at which a reaction fired.
 * Each value is the state of the reaction at that time.
 *
 * @returns the log in dictionary form
 */
- (NSDictionary*)log;

/**
 * Runs the simulation as described in the class-description.
 * Falls back on C code for performance reasons.
 */
- (void)runSimulation;

/**
 * Converts the log into a string in CSV format.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 *
 * @returns the log as a CSV string
 */
- (NSString*)logAsCSV:(NSNumber*)timestep;

/**
 * Converts the log into a string in CSV format.
 * Uses whitespace to pad the string in order to give it a more tabular
 * appearance if printed.
 *
 * @returns the log as a whitespace-padded CSV string
 */
- (NSString*)logAsPrettyCSV;

/**
 * Writes a summary of the simulation-evolution as a csv file.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 *
 * @param path the path at which to write the csv
 * @param timestep the intervals at which to observe the simulation
 */
- (void)writeLogToPath:(NSString*)path timestep:(NSNumber*)timestep;

/**
 * Plot the evolution of the simulation as png.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 *
 * @param path the path at which to write the png
 * @param timestep the intervals at which to observe the simulation
 * @bug does not work for very large logs (e.g. res/examples/dimer-decay.txt)
 * @see Gillespie#plotLogToPath:timestep:
 */
- (void)plotLogToPathUnsafe:(NSString*)path timestep:(NSNumber*)timestep;

/**
 * Plot the evolution of the simulation as png.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 * In order to avoid problems with large files, write the log to a temporary
 * file first.
 *
 * @param path the path at which to write the png
 * @param timestep the intervals at which to observe the simulation
 */
- (void)plotLogToPath:(NSString*)path timestep:(NSNumber*)timestep;

/**
 * Plot the evolution of the simulation as jpg.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 * In order to avoid problems with large files, write the log to a temporary
 * file first.
 * Uses gnuplot for plotting to png and then converts that file to jpg.
 *
 * @param path the path at which to write the png
 * @param timestep the intervals at which to observe the simulation
 */
- (void)plotLogToPathAsJpg:(NSString*)path timestep:(NSNumber*)timestep;

/**
 * Plot the evolution of the simulation as eps.
 * Observe the simulation at some regular interval given by timestep or or
 * after each state-change if timestep is nil.
 * In order to avoid problems with large files, write the log to a temporary
 * file first.
 *
 * @param path the path at which to write the png
 * @param timestep the intervals at which to observe the simulation
 */
- (void)plotLogToPathAsEps:(NSString*)path timestep:(NSNumber*)timestep;

/**
 * Initialises a new Gillespie object.
 *
 * @param state x_0
 * @param stoptime T
 * @param reactions {R_1,...,R_M}
 */
- (id)initWithState:(NSArray*)state
        andStoptime:(NSNumber*)stoptime
       andReactions:(NSArray*)reactions;

/**
 * Initialises a new autoreleased Gillespie object.
 *
 * @param state x_0
 * @param stoptime T
 * @param reactions {R_1,...,R_M}
 */
+ (Gillespie*)gillespieWithState:(NSArray*)state
                    withStoptime:(NSNumber*)stoptime
                   withReactions:(NSArray*)reactions;

@end
