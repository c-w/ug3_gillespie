#import "Gillespie.h"

@interface Gillespie(PrivateMethods)

- (double)calculate_a0;
- (int*)nextReactionToFire:(double)probabilitymass;
- (NSString*)logUsingColumnSeparator:(NSString*)cseparator
                  andRecordSeparator:(NSString*)rseparator
                         andTimestep:(NSNumber*)timestep;
- (void)plotLogToPath:(NSString*)path
             timestep:(NSNumber*)timestep
             terminal:(NSString*)terminal;

@end

@implementation Gillespie(PrivateMethods)

- (void)plotLogToPath:(NSString*)path
             timestep:(NSNumber*)timestep
             terminal:(NSString*)terminalstring {
    NSDate* date = [NSDate date];
    NSDateFormatter* dateformatter = [[NSDateFormatter alloc] init];
    [dateformatter setDateFormat:@"dd_MM_yy_HH_mm_ss"];
    NSString* datestring = [dateformatter stringFromDate:date];
    [dateformatter release];
    NSString* tmpfilepath =
        [NSString stringWithFormat:
            @".__tmp_Gillespie_plotLogToPath_%@_%f",
                datestring, random_double(0, 42)];
    NSArray* alphabet = [[_reactions objectAtIndex:0] alphabet];
    NSMutableString* plotcommand = [NSMutableString string];
    int len = [alphabet count];
    int i = 0;
    while (i < len) {
        NSString* functionname = [alphabet objectAtIndex:i];
        [plotcommand appendString:
            (i == 0) ?
                [NSString stringWithFormat:@"\"%@\"", tmpfilepath]
                : @"\"\""];
        [plotcommand appendString:
            [NSString stringWithFormat:
                @" using 1:%d title \"%@\"", i + 2, functionname]];
        [plotcommand appendString:(i != len - 1)?@",":@""];
        i++;
    }
    // create temp file with csv data
    [self writeLogToPath:tmpfilepath timestep:timestep];
    // call gnuplot 
    gnuplot_ctrl* h = gnuplot_init();
    gnuplot_cmd(h, "reset");
    gnuplot_cmd(h, "set terminal %s", [terminalstring UTF8String]);
    gnuplot_cmd(h, "set output '%s'", [path UTF8String]);
    gnuplot_cmd(h, "set key right center");
    gnuplot_cmd(h, "set ylabel 'molecule count'");
    gnuplot_cmd(h, "set xlabel 'time'");
    gnuplot_cmd(h, "set xrange [0:%f]", _stoptime);
    gnuplot_cmd(h, "set style data linespoints");
    gnuplot_cmd(h, "set datafile separator ','");
    gnuplot_cmd(h, "plot %s", [plotcommand UTF8String]);
    gnuplot_close(h);
    remove([tmpfilepath UTF8String]);
}

- (NSString*)logUsingColumnSeparator:(NSString*)cseparator
                  andRecordSeparator:(NSString*)rseparator
                         andTimestep:(NSNumber*)timestep {
    NSMutableString* logstring = [NSMutableString string];
    NSArray* sortedtimes = [[self log] sortedKeys];
    int len = [sortedtimes count];
    int i = 0;
    if (timestep == nil) {
        while (i < len) {
            NSNumber* time  = [sortedtimes objectAtIndex:i];
            NSString* state =
                [[[self log] objectForKey:time]
                    componentsJoinedByString:cseparator];
            [logstring appendFormat:@"%@%@%@%@",
                time, cseparator, state, rseparator];
            i++;
        }
    }
    else {
        double step = [timestep doubleValue];
        int taken = 1;
        double curtime = 0.0f;
        while (i < len) {
            NSNumber* time = [sortedtimes objectAtIndex:i];
            if ([time doubleValue] >= curtime) {
                NSString* state =
                    [[[self log] objectForKey:time]
                        componentsJoinedByString:cseparator];
                [logstring appendFormat:@"%f%@%@%@",
                    curtime, cseparator, state, rseparator];
                curtime = step * taken;
                taken++;
            }
            i++;
        }
        NSString* laststate =
            [[[self log] objectForKey:[sortedtimes lastObject]]
                componentsJoinedByString:cseparator];
        while (curtime < _stoptime) {
            [logstring appendFormat:@"%f%@%@%@",
                curtime, cseparator, laststate, rseparator];
            curtime = step * taken;
            taken++;
        }
        double lasttime = step * (taken - 2);
        if (_stoptime - lasttime > DBL_EPSILON) {
            [logstring appendFormat:@"%f%@%@",
                _stoptime, cseparator, laststate];
        }
    }
    if ([logstring hasSuffix:rseparator]) {
        [logstring deleteCharactersInRange:
            NSMakeRange([logstring length] - [rseparator length],
                        [rseparator length])];
    }
    return logstring;
}

- (double)calculate_a0 {
    double sum = 0.0;
    for (Reaction* reaction in _reactions) {
        sum += [reaction propensity:_state];
    }
    return sum;
}

- (int*)nextReactionToFire:(double)probabilitymass {
    double sum = 0.0;
    for (Reaction* reaction in _reactions) {
        sum += [reaction propensity:_state];
        if (sum > probabilitymass) {
            return [reaction statechangevector];
        }
    }
    return NULL;
}

@end

@implementation Gillespie

- (NSDictionary*)log {
    if ([_log count] > 0) {
        return _log;
    }
    // convert linked list to dict
    NSMutableDictionary* logtemp = [NSMutableDictionary dictionary];
    Node* head = _linkedlog;
    while (head) {
        NSArray* state =
            [NSArray arrayFromLongArray:head->state ofLength:_len];
        [logtemp setObject:state
                 forKey:[NSNumber numberWithDouble:head->time]];
        head = head->next;
    }
    _log = [[NSDictionary dictionaryWithDictionary:logtemp] retain];
    return _log;
}

- (void)runSimulation {
    Node* head = NULL;
    Node* curr = NULL;
    double t = 0.0f;
    double tau = 0.0f;
    double r1 = 0.0f;
    double r2 = 0.0f;
    double a0 = 0.0f;
    int* vj = NULL;
    int i = 0;
    // log state at time t = 0
    curr = (Node*)malloc(sizeof(Node));
    if (curr == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    curr->time = 0.0f;
    curr->state = copyLongArray(_state, _len); 
    curr->next = head;
    head = curr;
    // run simulation
    while (t < _stoptime) {
        r1 = random_double(0.0f, 1.0f);
        r2 = random_double(0.0f, 1.0f);
        // calculate value of a0
        // if a0 = 0, no more reactions can fire -> finish simulation early
        a0 = [self calculate_a0];
        if (a0 <= DBL_EPSILON) {
            break;
        }
        // update time by a random interval
        tau = 1.0 / a0 * log(1.0 / r1);
        t += tau;
        // update state by firing a random reaction
        vj = [self nextReactionToFire:a0*r2];
        if (vj != NULL) {
            i = 0;
            while (i < _len) {
                _state[i] += (long)vj[i];
                i++;
            }
            // log state at time t
            curr = (Node*)malloc(sizeof(Node));
            if (curr == NULL) {
                fprintf(stderr, "critical error: malloc failed in function "
                                "%s\n", __FUNCTION__);
                exit(EXIT_FAILURE);
            }
            curr->time = t;
            curr->state = copyLongArray(_state, _len); 
            curr->next = head;
            head = curr;
        }
    }
    // sometimes a couple of stray reactions after stoptime get fired
    // quick-and-dirty hack to remove these events
    Node* temp = NULL;
    while (head) { // to make sure we never dereference a null pointer
        if (head->time > _stoptime) {
            temp = head->next;
            free(head->state);
            free(head);
            head = temp;
        }
        else {
            break;
        }
    }
    // log state at time t = _stoptime
    curr = (Node*)malloc(sizeof(Node));
    if (curr == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    curr->time = _stoptime;
    curr->state = copyLongArray(_state, _len);
    curr->next = head;
    head = curr;
    // keep a pointer to the linked list
    _linkedlog = head;
}

- (NSString*)logAsPrettyCSV {
    int TAB_LEN = 4;
    // find longest string in each column of the log
    NSArray* sortedkeys = [[self log] sortedKeys];
    int ncols = [[[self log] objectForKey:[sortedkeys lastObject]] count] + 1;
    int* maxcolumnlens = (int*)malloc(sizeof(int) * ncols);
    if (maxcolumnlens == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    int i = 0;
    while (i < ncols) {
        maxcolumnlens[i] = 0;
        i++;
    }
    for (NSNumber* time in sortedkeys) {
        int firstcolumnlen = [[time description] length];
        if (maxcolumnlens[0] < firstcolumnlen) {
            maxcolumnlens[0] = firstcolumnlen;
        } 
        NSArray* othercols = [[self log] objectForKey:time];
        int j = 1;
        for (NSNumber* moleculecount in othercols) {
            int columnlen = [[moleculecount description] length];
            if (maxcolumnlens[j] < columnlen) {
                maxcolumnlens[j] = columnlen;
            }
            i++;
        }
    }
    // pad each line to make a nice tabular-style string out of the csv
    NSMutableArray* prettscsvlines =
        [NSMutableArray arrayWithCapacity:[[self log] count]];
    for (NSNumber* time in sortedkeys) {
        NSString* timestring = [time description];
        int tpad = maxcolumnlens[0] - [timestring length] + TAB_LEN;
        NSString* paddedtime =
            [timestring stringByPaddingRightAndPrepending:@","
                                                padAmount:tpad
                                               withString:@" "];
        int k = 1;
        NSArray* othercols = [[self log] objectForKey:time];
        NSMutableString* paddedmoleculecounts = [NSMutableString string];
        for (NSNumber* mcount in othercols) {
            NSString* mcountstring = [mcount description];
            int mpad = maxcolumnlens[k] - [mcountstring length] + TAB_LEN;
            NSString* prepend = (k == ncols - 1) ? @"" : @",";
            NSString* paddedmcount =
                [mcountstring stringByPaddingRightAndPrepending:prepend
                                                      padAmount:mpad
                                                     withString:@" "];
            [paddedmoleculecounts appendString:paddedmcount];
            k++;
        }
        [prettscsvlines addObject:
            [paddedtime stringByAppendingString:paddedmoleculecounts]];
    }
    // prepend column labels
    NSMutableString* headerstring = [NSMutableString stringWithString:@"# t "];
    int timepad = maxcolumnlens[0] - [headerstring length] + TAB_LEN + 1;
    [headerstring appendString:
        [[NSString string] stringByPaddingToLength:timepad
                                        withString:@" "
                                   startingAtIndex:0]];
    NSArray* alphabet = [[_reactions lastObject] alphabet];
    int totallen = maxcolumnlens[0] + TAB_LEN;
    int l = 1;
    for (NSString* alphabetstring in alphabet) {
        int apad = maxcolumnlens[l] - [alphabetstring length] + TAB_LEN;
        NSString* prepend = (l == ncols - 1) ? @"" : @" ";
        NSString* paddedalphabetstring =
            [alphabetstring stringByPaddingRightAndPrepending:prepend
                                                    padAmount:apad
                                                   withString:@" "];
        [headerstring appendString:paddedalphabetstring];
        totallen += maxcolumnlens[l] + TAB_LEN;
        l++;
    }
    NSString* separatorstring =
        [[@"#" stringByPaddingRightAndPrepending:nil
                                      padAmount:totallen + TAB_LEN
                                     withString:@"-"]
        stringByAppendingString:@"#"];
    [headerstring setString:
        [NSString stringWithFormat:@"%@\n%@\n%@\n",
            separatorstring, headerstring, separatorstring]];
    // put it all together, clean-up, and return
    NSString* prettycsvstring =
        [headerstring stringByAppendingString:
            [prettscsvlines componentsJoinedByString:@"\n"]];
    free(maxcolumnlens); 
    return prettycsvstring;
}

- (NSString*)logAsCSV:(NSNumber*)timestep {
    NSString* alphabet =
        [[[_reactions objectAtIndex:0] alphabet]
            componentsJoinedByString:@","];
    NSString* logstring = 
        [NSString stringWithFormat:@"#t,%@\n%@",
            alphabet,
            [self logUsingColumnSeparator:@","
                       andRecordSeparator:@"\n"
                              andTimestep:timestep]];
    return logstring;
}

- (void)writeLogToPath:(NSString*)path timestep:(NSNumber*)timestep {
    [[self logAsCSV:timestep] writeToPath:path];
}

- (void)plotLogToPathAsJpg:(NSString*)path timestep:(NSNumber*)timestep {
    [self plotLogToPath:path timestep:timestep];
    NSString* convertcmd =
        [NSString stringWithFormat:@"convert -format jpg \"%@\" \"%@\"",
            path, path];
    int result = system([convertcmd UTF8String]);
    if (result == -1 ) {
        fprintf(stderr, "critical error: system returned -1 in %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
}

- (void)plotLogToPathAsEps:(NSString*)path timestep:(NSNumber*)timestep {
    NSString* terminalstring = @"postscript eps size 3.5,2.62 enhanced color "
                                "font 'Helvetica,20' linewidth 2";
    [self plotLogToPath:path timestep:timestep terminal:terminalstring];
}

- (void)plotLogToPath:(NSString*)path timestep:(NSNumber*)timestep {
    [self plotLogToPath:path timestep:timestep terminal:@"png"];
}

- (void)plotLogToPathUnsafe:(NSString*)path timestep:(NSNumber*)timestep {
    NSString* data =
        [self logUsingColumnSeparator:@","
                   andRecordSeparator:@" "
                          andTimestep:timestep];
    NSArray* alphabet = [[_reactions objectAtIndex:0] alphabet];
    NSMutableString* plotcommand = [NSMutableString string];
    int len = [alphabet count];
    int i = 0;
    while (i < len) {
        NSString* functionname = [alphabet objectAtIndex:i];
        [plotcommand appendString:(i == 0)?@"":@"\"\""];
        [plotcommand appendString:
            [NSString stringWithFormat:
                @" using 1:%d title \"%@\"", i + 2, functionname]];
        [plotcommand appendString:(i != len - 1)?@",":@""];
        i++;
    }
    // call gnuplot 
    gnuplot_ctrl* h = gnuplot_init();
    gnuplot_cmd(h, "reset");
    gnuplot_cmd(h, "set terminal png");
    gnuplot_cmd(h, "set output '%s'", [path UTF8String]);
    gnuplot_cmd(h, "set key right center");
    gnuplot_cmd(h, "set xlabel 'time'");
    gnuplot_cmd(h, "set xrange [0:%f]", _stoptime);
    gnuplot_cmd(h, "set style data linespoints");
    gnuplot_cmd(h, "plot \"< echo -e '%s' | tr ' ' '\\n' | tr ',' ' '\" %s",
                [data UTF8String], [plotcommand UTF8String]);
    gnuplot_close(h);
}

- (id)initWithState:(NSArray*)state
        andStoptime:(NSNumber*)stoptime
       andReactions:(NSArray*)reactions {
    self = [super init];
    if (!self) {
        return nil;
    }
    _state = [state toLongArray];
    _len = [state count];
    _stoptime = [stoptime doubleValue];
    _reactions = [reactions retain];
    _log = nil;
    _linkedlog = NULL;
    return self;
}

+ (Gillespie*)gillespieWithState:(NSArray*)state
                    withStoptime:(NSNumber*)stoptime
                   withReactions:(NSArray*)reactions {
    Gillespie* gillespie =
        [[Gillespie alloc] initWithState:state
                                      andStoptime:stoptime
                                     andReactions:reactions];
    return [gillespie autorelease];
}

// disable default constructor
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self release];
    [super doesNotRecognizeSelector:_cmd];
    return nil; 
}

- (void)dealloc {
    free(_state);
    [_reactions release];
    [_log release];
    Node* temp = NULL;
    while (_linkedlog) {
        temp = _linkedlog->next;
        free(_linkedlog->state);
        free(_linkedlog);
        _linkedlog = temp;
    }
    [super dealloc];
}

@end
