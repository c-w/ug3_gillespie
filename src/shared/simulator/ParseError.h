/**
 * A class representing an error occuring while parsing a SimulationScript.
 */
@interface ParseError : NSObject {
    NSNumber* _code;
    NSString* _info;
    NSString* _file;
    NSNumber* _lineno;
    NSString* _line;
}

/**
 * The error-code of the invoking ParseError.
 */
- (NSInteger)code;

/**
 * The information-string of the invoking ParseError.
 */
- (NSString*)info;

/**
 * The file in which the invoking ParseError occured.
 */
- (NSString*)file;

/**
 * The line on which the invoking ParseError occured.
 */
- (NSString*)line;

/**
 * The line number on which the invoking ParseError occured.
 */
- (NSInteger)lineno;

/**
 * A pretty error message for the invoking ParseError.
 * This should be used to print ParseErrors.
 */
- (NSString*)errorMessage;

/**
 * Set the file in which the ParseError occured.
 */
- (void)setFile:(NSString*)file;

/**
 * Set the line on which the ParseError occured.
 */
- (void)setLine:(NSString*)line;

/**
 * Set the line number which the ParseError occured.
 */
- (void)setLineno:(NSInteger)lineno;

/**
 * Could not find file.
 */
+ (ParseError*)fileNotFound;

/**
 * File contains unparsable line.
 */
+ (ParseError*)unrecognizedLineType;

/**
 * Function declaration contains unparsable token.
 */
+ (ParseError*)invalidTokenInFunctionDeclaration;

/**
 * Assignment has missing operands (e.g. 'X=' or '=10').
 */
+ (ParseError*)assignmentWithTooFewOperands;

/**
 * Assignment has too many operands (e.g. 'X=10=D').
 */
+ (ParseError*)assignmentWithTooManyOperands;

/**
 * Assignment is to non numeric quantity (e.g. 'X=foo').
 */
+ (ParseError*)assignmentToNonNumeric;

/**
 * Assignment is to negative quantity (e.g. 'X=-5').
 */
+ (ParseError*)assignmentToNegative;

/**
 * Assignment is to too big or too small quantity (e.g. 'X=1e2000').
 */
+ (ParseError*)assignmentOverflow;

/**
 * File contains multiple declarations of stop-time 't'.
 */
+ (ParseError*)multipleDeclarationsStoptime;

/**
 * File does not declare stop-time 't'.
 */
+ (ParseError*)noDeclarationStoptime;

/**
 * File has multiple declarations of the same molecule.
 */
+ (ParseError*)multipleDeclarationsMolecule;

/**
 * File does not declare any molecules.
 */
+ (ParseError*)noDeclarationMolecule;

/**
 * File declares a molecule but never uses it.
 */
+ (ParseError*)moleculeUnused;

/**
 * File has multiple declarations of the same reaction-rate.
 */
+ (ParseError*)multipleDeclarationsReactionrate;

/**
 * File does not declare any reaction-rates.
 */
+ (ParseError*)noDeclarationReactionrate;

/**
 * File declares a reaction-rate but never uses it.
 */
+ (ParseError*)reactionrateUnused;

/**
 * File has multiple declarations of the same reaction.
 */
+ (ParseError*)multipleDeclarationsReaction;

/**
 * File does not declare any reactions.
 */
+ (ParseError*)noDeclarationReaction;

/**
 * File declares a reaction but no corresponding reaction-rate.
 */
+ (ParseError*)reactionWithoutReactionrate;

/**
 * File declares a reaction involving an undeclared molecule.
 */
+ (ParseError*)reactionWithUndeclaredMolecule;

@end

enum ParseErrorCode {
    ParseErrorCode_NoError = 0,

    ParseErrorCode_FileNotFound,
    ParseErrorCode_UnrecognizedLineType,

    ParseErrorCode_InvalidTokenInFunctionDeclaration,
    ParseErrorCode_AssignmentWithTooFewOperands,
    ParseErrorCode_AssignmentWithTooManyOperands,
    ParseErrorCode_AssignmentToNonNumeric,
    ParseErrorCode_AssignmentToNegative,
    ParseErrorCode_AssignmentOverflow,
    
    ParseErrorCode_MultipleDeclarationsStoptime,
    ParseErrorCode_NoDeclarationStoptime,

    ParseErrorCode_MultipleDeclarationsMolecule,
    ParseErrorCode_NoDeclarationMolecule,
    ParseErrorCode_MoleculeUnused,

    ParseErrorCode_MultipleDeclarationsReactionrate,
    ParseErrorCode_NoDeclarationReactionrate,
    ParseErrorCode_ReactionrateUnused,

    ParseErrorCode_MultipleDeclarationsReaction,
    ParseErrorCode_NoDeclarationReaction,
    ParseErrorCode_ReactionWithoutReactionrate,
    ParseErrorCode_ReactionWithUndeclaredMolecule
};
