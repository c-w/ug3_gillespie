#import "ParseError.h"

@interface ParseError(PrivateMethods)

+ (NSString*)errorCodeToErrorString:(NSNumber*)errorCode;
- (id)initWithCode:(NSInteger)code;
- (id)initWithNumber:(NSNumber*)code;
- (id)initWithNumber:(NSNumber*)code andFile:(NSString*)file;
- (id)initWithNumber:(NSNumber*)code
             andFile:(NSString*)file
             andLine:(NSString*)line
             andLineno:(NSNumber*)lineno;

@end

@implementation ParseError(PrivateMethods)

+ (NSString*)errorCodeToErrorString:(NSNumber*)errorCode {
    NSInteger code = [errorCode integerValue];
    NSString* errorstring = nil;
    switch (code) {
        case ParseErrorCode_NoError:
            errorstring = @"no error";
            break;
        case ParseErrorCode_FileNotFound:
            errorstring = @"file not found";
            break;
        case ParseErrorCode_UnrecognizedLineType:
            errorstring = @"unparsable line";
            break;
        case ParseErrorCode_AssignmentWithTooFewOperands:
            errorstring = @"assignment has too few operands";
            break;
        case ParseErrorCode_AssignmentWithTooManyOperands:
            errorstring = @"assignment has too many operands";
            break;
        case ParseErrorCode_AssignmentToNonNumeric:
            errorstring = @"assignment to non-numeric";
            break;
        case ParseErrorCode_AssignmentToNegative:
            errorstring = @"assignment to negative quantity";
            break;
        case ParseErrorCode_AssignmentOverflow:
            errorstring = @"assignment to too big or too small quantity";
            break;
        case ParseErrorCode_MultipleDeclarationsStoptime:
            errorstring = @"stop-time defined multiple times";
            break;
        case ParseErrorCode_NoDeclarationStoptime:
            errorstring = @"stop-time undeclared";
            break;
        case ParseErrorCode_MultipleDeclarationsMolecule:
            errorstring = @"assignment of previously defined molecule";
            break;
        case ParseErrorCode_NoDeclarationMolecule:
            errorstring = @"no initial molecule counts declared";
            break;
        case ParseErrorCode_MultipleDeclarationsReactionrate:
            errorstring = @"assignment of previously defined reaction-rate";
            break;
        case ParseErrorCode_NoDeclarationReactionrate:
            errorstring = @"no reaction-rates declared";
            break;
        case ParseErrorCode_MultipleDeclarationsReaction:
            errorstring = @"assignment of previously defined reaction";
            break;
        case ParseErrorCode_NoDeclarationReaction:
            errorstring = @"no reaction declared";
            break;
        case ParseErrorCode_InvalidTokenInFunctionDeclaration:
            errorstring = @"invalid token in function declaration";
            break;
        case ParseErrorCode_ReactionWithoutReactionrate:
            errorstring = @"no reaction-rate declared for reaction";
            break;
        case ParseErrorCode_MoleculeUnused:
            errorstring = @"molecule declared but never used";
            break;
        case ParseErrorCode_ReactionrateUnused:
            errorstring = @"reaction-rate declared but never used";
            break;
        case ParseErrorCode_ReactionWithUndeclaredMolecule:
            errorstring = @"reaction uses undeclared molecule";
            break;
        default:
            [[NSException exceptionWithName:@"NoStringForErrorCodeException"
                                     reason:
                 [NSString stringWithFormat:@"error %d out of range", code]
                                   userInfo:nil]
            raise]; 
            break;
    }
    return errorstring;
}

- (id)initWithCode:(NSInteger)code {
    return [self initWithNumber:[NSNumber numberWithInteger:code]];
}

- (id)initWithNumber:(NSNumber*)code {
    return [self initWithNumber:code andFile:nil andLine:nil andLineno:nil];
}

- (id)initWithNumber:(NSNumber*)code andFile:(NSString*)file {
    return [self initWithNumber:code andFile:file andLine:nil andLineno:nil];
}

- (id)initWithNumber:(NSNumber*)code
            andFile:(NSString*)file
            andLine:(NSString*)line
            andLineno:(NSNumber*)lineno {
    self = [super init];
    if (self) {
        _code = [code retain];
        _info = [[ParseError errorCodeToErrorString:_code] retain];
        _file = [file retain];
        _line = [line retain];
        _lineno = [lineno retain];
    }
    return self;
}

@end

@implementation ParseError

// disable default constructor
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self release];
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)dealloc {
    [_code release];
    [_info release];
    [_file release];
    [_line release];
    [_lineno release];
    [super dealloc];
}

- (NSString*)description {
    NSMutableString* descr = [NSMutableString stringWithString:@"\n"];
    if (_code != nil) {
        [descr appendFormat:@"\tcode=%@\n", _code];
    }
    if (_info != nil) {
        [descr appendFormat:@"\tinfo=%@\n", _info];
    }
    if (_file != nil) {
        [descr appendFormat:@"\tfile=%@\n", _file];
    }
    if (_line != nil) {
        [descr appendFormat:@"\tline=%@\n", _line];
    }
    if (_lineno != nil) {
        [descr appendFormat:@"\tlineno=%@\n", _lineno];
    }
    NSString* description =
        [NSString stringWithFormat:@"ParseError{%@}", descr];
    return description;
}

- (NSString*)errorMessage {
    NSString* linenostring =
        _lineno == nil ? @"" :
            [NSString stringWithFormat:@" on line %@", _lineno];
    NSString* linestring =
        _line == nil ? @"" :
            [NSString stringWithFormat:@" [%@]", _line];
    NSString* errormessage =
        [NSString stringWithFormat:@"parsing '%@'%@%@: %@",
            _file, linenostring, linestring, _info];
    return errormessage;
}

- (BOOL)isEqual:(id)other {
    if (other == self) {
        return YES;
    }
    if (!other || ![other isKindOfClass:[self class]]) {
        return NO;
    }
    return [self code] == [other code];
}

- (NSInteger)code {
    return [_code intValue];
}

- (NSString*)info {
    return _info;
}

- (NSString*)file {
    return _file;
}

- (NSString*)line {
    return _line;
}

- (NSInteger)lineno {
    return [_lineno intValue];
}

- (void)setFile:(NSString*)file {
    if (_file == nil) {
        _file = [file retain];
    }
}

- (void)setLine:(NSString*)line {
    if (_line == nil) {
        _line = [line retain];
    }
}

- (void)setLineno:(NSInteger)lineno {
    if (_lineno == nil) {
        _lineno = [[NSNumber numberWithInteger:lineno] retain];
    }
}

+ (ParseError*)fileNotFound {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_FileNotFound]
        autorelease];
}

+ (ParseError*)unrecognizedLineType {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_UnrecognizedLineType]
        autorelease];
}

+ (ParseError*)assignmentWithTooFewOperands {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_AssignmentWithTooFewOperands]
        autorelease];
}

+ (ParseError*)assignmentWithTooManyOperands {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_AssignmentWithTooManyOperands]
        autorelease];
}

+ (ParseError*)assignmentToNonNumeric {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_AssignmentToNonNumeric]
        autorelease];
}

+ (ParseError*)assignmentToNegative {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_AssignmentToNegative]
        autorelease];
}

+ (ParseError*)assignmentOverflow {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_AssignmentOverflow]
        autorelease];
}

+ (ParseError*)multipleDeclarationsStoptime {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_MultipleDeclarationsStoptime]
        autorelease];
}

+ (ParseError*)noDeclarationStoptime {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_NoDeclarationStoptime]
        autorelease];
}

+ (ParseError*)multipleDeclarationsMolecule {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_MultipleDeclarationsMolecule]
        autorelease];
}

+ (ParseError*)noDeclarationMolecule {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_NoDeclarationMolecule]
        autorelease];
}

+ (ParseError*)multipleDeclarationsReactionrate {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_MultipleDeclarationsReactionrate]
        autorelease];
}

+ (ParseError*)noDeclarationReactionrate {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_NoDeclarationReactionrate]
        autorelease];
}

+ (ParseError*)multipleDeclarationsReaction {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_MultipleDeclarationsReaction]
        autorelease];
}

+ (ParseError*)noDeclarationReaction {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_NoDeclarationReaction]
        autorelease];
}

+ (ParseError*)invalidTokenInFunctionDeclaration {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_InvalidTokenInFunctionDeclaration]
        autorelease];
}

+ (ParseError*)reactionWithoutReactionrate {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_ReactionWithoutReactionrate]
        autorelease];
}

+ (ParseError*)moleculeUnused {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_MoleculeUnused]
        autorelease];
}

+ (ParseError*)reactionrateUnused {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_ReactionrateUnused]
        autorelease];
}

+ (ParseError*)reactionWithUndeclaredMolecule {
    return
        [[[self alloc]
            initWithCode:ParseErrorCode_ReactionWithUndeclaredMolecule]
        autorelease];
}

@end
