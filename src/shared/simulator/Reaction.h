#import "Constants.h"

/**
 * A class representing a Reaction-channel in the Gillespie algorithm.
 */
@interface Reaction : TestableObject {
    NSArray* _alphabet;
    double _rate;
    int* _statechangevector;
    size_t _len;
    BOOL _isDimerisation;
    BOOL _isCreation;
    NSString* _name;
}


/**
 * The set of all molecules in the world of the invoking reaction.
 */
- (NSArray*)alphabet;

/**
 * The kinetic constant of the invoking reaction.
 */
- (double)rate;

/**
 * The state-change-vector of the invoking reaction.
 */
- (int*)statechangevector;

/**
 * The length of the statechangevector of the invoking reaction.
 */
- (size_t)len;

/**
 * @returns YES if the reaction is a dimerisation reaction or NO otherwise
 */
- (BOOL)isDimerisation;

/**
 * @returns YES if the reaction is a creation reaction or NO otherwise
 */
- (BOOL)isCreation;

/**
 * The name of the reaction.
 */
- (NSString*)name;

/**
 * Calculate the propensity p of the invoking reaction in some state.
 * Let |X| be the number of molecules of type X in the state.
 * Let c be the kinetic constant of the reaction.
 * For a reaction:
 * - 'A + B -> C' --> p = c * |A| * |B|.
 * - 'A + A -> C' --> p = c * |A| * (|A| - 1) * (1/2).
 * - '-> C' --> p = c.
 *
 * @param population the state for which to calculate the propensity
 * @returns the propensity of the reaction in this state
 */
- (double)propensity:(long*)population;

/**
 * Initialises a new Reaction object by parsing a string.
 *
 * @param alphabet the set of all molecules
 * @param expression the expression of the reaction
 * @param rate the kinetic constant of the reaction
 * @param name the name of the reaction
 * @returns a new autoreleased Reaction
 */
- (id)initWithAlphabet:(NSArray*)alphabet
         andExpression:(NSString*)expression
               andRate:(double)rate
               andName:(NSString*)name;

/**
 * Create an autoreleased Reaction object by parsing a string.
 *
 * @param alphabet the set of all molecules
 * @param expression the expression of the reaction
 * @param rate the kinetic constant of the reaction
 * @param name the name of the reaction
 * @returns a new autoreleased Reaction
 */
+ (Reaction*)reactionOverAlphabet:(NSArray*)alphabet
                   withExpression:(NSString*)expression
                         withRate:(double)rate
                         withName:(NSString*)name;

@end
