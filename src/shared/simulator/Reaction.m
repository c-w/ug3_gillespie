#import "Reaction.h"

@interface Reaction(PrivateMethods)

+ (BOOL)expressionIsCreation:(NSString*)expression;
+ (BOOL)expressionIsDimerisation:(NSString*)expression;

@end

@implementation Reaction(PrivateMethods)

+ (BOOL)expressionIsCreation:(NSString*)expression {
    // check for dimerisation function i.e. -> Y
    NSString* lefthandside =
        [[expression componentsSeparatedByString:FUNCTION_RESULT_OPERATOR]
        objectAtIndex:0];
    if ([lefthandside isEqualToString:@""]) {
        return YES;
    }
    return NO;
}

+ (BOOL)expressionIsDimerisation:(NSString*)expression {
    // check for dimerisation function i.e. X + X -> Y
    NSString* lefthandside =
        [[expression componentsSeparatedByString:FUNCTION_RESULT_OPERATOR]
        objectAtIndex:0];
    NSArray* tokens =
        [lefthandside nonEmptyComponentsSeparatedByString:
            FUNCTION_COMBINATION_OPERATOR];
    if ([tokens count] == 2 &&
        [[tokens objectAtIndex:0] isEqualToString:[tokens objectAtIndex:1]]) {
        return YES;
    }
    return NO;
}

- (void)setStatechangevectorOverAlphabet:(NSArray*)alphabet
                          fromExpression:(NSString*)expression {
}

@end

@implementation Reaction

- (NSArray*)alphabet {
    return _alphabet;
}

- (double)rate {
    return _rate;
}

- (int*)statechangevector {
    return _statechangevector;
}

- (size_t)len {
    return _len;
}

- (BOOL)isCreation {
    return _isCreation;
}

- (BOOL)isDimerisation {
    return _isDimerisation;
}

- (NSString*)name {
    return _name;
}

+ (Reaction*)reactionOverAlphabet:(NSArray*)alphabet
                   withExpression:(NSString*)expression
                         withRate:(double)rate
                         withName:(NSString*)name {
    Reaction* reaction =
        [[Reaction alloc] initWithAlphabet:alphabet
                                      andExpression:expression
                                            andRate:rate
                                            andName:name];
    return [reaction autorelease];
}

- (id)initWithAlphabet:(NSArray*)alphabet
         andExpression:(NSString*)expression
               andRate:(double)rate
               andName:(NSString*)name {
    self = [super init];
    if (self == nil) {
        return nil;
    }
    _alphabet = [alphabet retain];
    _rate = rate;
    _isDimerisation = [Reaction expressionIsDimerisation:expression];
    _isCreation = [Reaction expressionIsCreation:expression];
    _name = [name retain];
    // set state-change vector
    // convert "A + B -> C" to [-1, -1, +1]
    NSMutableDictionary* ruledict =
        [NSMutableDictionary dictionaryWithCapacity:[alphabet count]];
    for (NSString* molecule in alphabet) {
        [ruledict setObject:[NSNumber zero] forKey:molecule];
    }
    NSArray* lhs_rhs =
        [expression componentsSeparatedByString:FUNCTION_RESULT_OPERATOR];
    NSArray* lhstokens =
        [[lhs_rhs objectAtIndex:0]
            nonEmptyComponentsSeparatedByString:FUNCTION_COMBINATION_OPERATOR];
    // elements on the left hand side of the reaction are decremented
    for (NSString* reactand in lhstokens) {
        int value = [[ruledict objectForKey:reactand] intValue];
        NSNumber* decrement = [NSNumber numberWithInt:value - 1];
        [ruledict setObject:decrement forKey:reactand];
    }
    NSArray* rhstokens =
        [[lhs_rhs objectAtIndex:1]
            nonEmptyComponentsSeparatedByString:FUNCTION_COMBINATION_OPERATOR];
    // elements on the right hand side of the reaction are incremented
    for (NSString* resultand in rhstokens) {
        int value = [[ruledict objectForKey:resultand] intValue];
        NSNumber* increment = [NSNumber numberWithInt:value + 1];
        [ruledict setObject:increment forKey:resultand];
    } 
    NSMutableArray* rulearray =
        [NSMutableArray arrayWithCapacity:[alphabet count]];
    for (NSString* molecule in alphabet) {
        [rulearray addObject:[ruledict objectForKey:molecule]];
    }
    _len = [rulearray count];
    _statechangevector = [rulearray toIntArray];
    return self;
}

// disable default constructor
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self release];
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)dealloc {
    free(_statechangevector);
    [_name release];
    [super dealloc];
}

- (NSString*)description {
    NSString* description =
        [NSString stringWithFormat:@"reaction %@ = %@, rate:%f",
                    _name, arrayToNSString(_statechangevector, _len), _rate];
    return description;
}

- (BOOL)isEqual:(id)other {
    if (other == self) {
        return YES;
    }
    if (!other || ![other isKindOfClass:[self class]]) {
        return NO;
    }
    if ((float)[self rate] != (float)[other rate]) {
        return NO;
    }
    return arraysEqual(_statechangevector, _len, [other statechangevector],
                       [other len]);
}

- (NSComparisonResult)compare:(Reaction*)other {
    return [[self name] compare:[other name]];
}

- (double)propensity:(long*)population {
    if (_isCreation) {
        return _rate;
    }
    double probability = _rate;
    int i = 0;
    while (i < _len) {
        int modifier = _statechangevector[i];
        if (modifier != 0) {
            long oldcount = population[i];
            long newcount = oldcount + modifier;
            if (newcount < 0) {
                return 0.0f;
            }
            if (newcount < oldcount) {
                if (_isDimerisation) {
                    return _rate * 0.5f * oldcount * (oldcount - 1);
                }
                probability *= oldcount; 
            }
        }
        i++;
    }
    return probability;
}

+ (void)load {
    [Reaction registerSubclass:self];
}

+ (BOOL)unitTest {
    print(@"Running unit-tests for %s", __FILE__);
    NSArray* alphabet = [NSArray arrayWithObjects:@"A", @"B", @"C", nil];
    // test for string-to-rule conversion
    Reaction* nreaction =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"A+B->C"
                              withRate:0.0f
                              withName:@"test"];
    int* nrule = [nreaction statechangevector];
    size_t nrulelen = [nreaction len];
    int nruleexpected[3] = {-1, -1, 1};
    if (!arraysEqual(nrule, nrulelen, nruleexpected, 3)) {
        print(@"--> problem with string-to-rule/array conversion: "
               "should give %@ but is %@",
               arrayToNSString(nruleexpected, 3),
               arrayToNSString(nrule, nrulelen));
        return NO;
    }
    Reaction* dreaction =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"A+A->C"
                              withRate:0.0f
                              withName:@"test"];
    int* drule = [dreaction statechangevector];
    size_t drulelen = [dreaction len];
    int druleexpected[3] = {-2, 0, 1};
    if (!arraysEqual(drule, drulelen, druleexpected, 3)) {
        print(@"--> problem with string-to-rule/array conversion: "
               "should give %@ but is %@",
               arrayToNSString(druleexpected, 3),
               arrayToNSString(drule, drulelen));
        return NO;
    }
    Reaction* creaction =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"->C"
                              withRate:0.0f
                              withName:@"test"];
    int* crule = [creaction statechangevector];
    size_t crulelen = [creaction len];
    int cruleexpected[3] = {0, 0, 1};
    if (!arraysEqual(crule, crulelen, cruleexpected, 3)) {
        print(@"--> problem with string-to-rule/array conversion: "
               "should give %@ but is %@",
               arrayToNSString(cruleexpected, 3),
               arrayToNSString(crule, crulelen));
        return NO;
    }
    Reaction* xreaction =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"A->"
                              withRate:0.0f
                              withName:@"test"];
    int* xrule = [xreaction statechangevector];
    size_t xrulelen = [xreaction len];
    int xruleexpected[3] = {-1, 0, 0};
    if (!arraysEqual(xrule, xrulelen, xruleexpected, 3)) {
        print(@"--> problem with string-to-rule/array conversion: "
               "should give %@ but is %@",
               arrayToNSString(xruleexpected, 3),
               arrayToNSString(xrule, xrulelen));
        return NO;
    }
    // test for dimerisation detection
    if ([Reaction expressionIsDimerisation:@"A->C"] ||
        [Reaction expressionIsDimerisation:@"->C"] ||
        [Reaction expressionIsDimerisation:@"A->"]) {
        print(@"--> problem with dimerisation detection: false positive");
        return NO;
    }
    if (![Reaction expressionIsDimerisation:@"A+A->C"]) {
        print(@"--> problem with dimerisation detection: false negative");
        return NO;
    }
    // test for creation detection
    if ([Reaction expressionIsCreation:@"A->C"] ||
        [Reaction expressionIsCreation:@"A+B->C"] ||
        [Reaction expressionIsCreation:@"A->"]) {
        print(@"--> problem with creation detection: false positive");
        return NO;
    }
    if (![Reaction expressionIsCreation:@"->C"]) {
        print(@"--> problem with creation detection: false negative");
        return NO;
    }
    // test for probability calculation
    long population530[3] = {5, 3, 0};
    long population030[3] = {0, 3, 0};
    long population130[3] = {1, 3, 0};
    Reaction* reaction =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"A+B->C"
                              withRate:0.25f
                               withName:@"normal"];
    double normalprob = [reaction propensity:population530];
    double normalprobexpected = 3.75f;
    if ((float)normalprob != (float)normalprobexpected) {
        print(@"--> problem with probability calculation: "
               "should give %f but is %f",
               normalprobexpected, normalprob);
        return NO;
    }
    double zeroprob = [reaction propensity:population030];
    double zeroprobexpected = 0.0f;
    if ((float)zeroprob != (float)zeroprobexpected) {
        print(@"--> problem with probability calculation: "
               "should give %f but is %f",
               zeroprobexpected, zeroprob);
        return NO;
    }
    Reaction* creation =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"->C"
                              withRate:0.5f
                               withName:@"creation"];
    double creationprob = [creation propensity:population530];
    double creationprobexpected = 0.5f;
    if ((float)creationprob != (float)creationprobexpected) {
        print(@"--> problem with probability calculation for creations: "
               "should give %f but is %f",
               creationprobexpected, creationprob);
        return NO;
    }
    Reaction* dimerisation =
        [Reaction reactionOverAlphabet:alphabet
                        withExpression:@"A+A->C"
                              withRate:0.85f
                               withName:@"dimerisation"];
    double dimerisationprob = [dimerisation propensity:population530];
    double dimerisationprobexpected = 8.5f;
    if ((float)dimerisationprob != (float)dimerisationprobexpected) {
        print(@"--> problem with probability calculation: "
               "should give %f but is %f",
               dimerisationprobexpected, dimerisationprob);
        return NO;
    }
    double dimerisationzeroprob = [dimerisation propensity:population130];
    double dimerisationzeroprobexpected = 0.0f;
    if ((float)dimerisationzeroprob != (float)dimerisationzeroprobexpected) {
        print(@"--> problem with probability calculation: "
               "should give %f but is %f",
               dimerisationzeroprobexpected, dimerisationzeroprob);
        return NO;
    }
    double dimerisationzeroprob2 = [dimerisation propensity:population030];
    double dimerisationzeroprobexpected2 = 0.0f;
    if ((float)dimerisationzeroprob2 != (float)dimerisationzeroprobexpected2) {
        print(@"--> problem with probability calculation: "
               "should give %f but is %f",
               dimerisationzeroprobexpected2, dimerisationzeroprob2);
        return NO;
    }
    return YES;
}

@end
