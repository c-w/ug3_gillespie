#import "Assignment.h"
#import "Constants.h"
#import "ParseError.h"
#import "Reaction.h"

/**
 * A class representing a configuration file for a Gillespie simulation.
 */
@interface SimulationScript : TestableObject {
    NSString* _filepath;
    NSString* _filecontents;
    NSArray* _lines;
    NSNumber* _stoptime;
    NSArray* _molecules;
    NSArray* _initialcounts;
    NSArray* _reactions;
}

/**
 * The path to the configuration-file.
 */
- (NSString*)filepath;

/**
 * The text read while the configuration-file was parsed.
 */
- (NSString*)filecontents;

/**
 * An array of lines in the configuration-file.
 */
- (NSArray*)lines;

/**
 * The stop-time parsed from the configuration file.
 */
- (NSNumber*)stoptime;

/**
 * The molecules parsed from the configuration file.
 */
- (NSArray*)molecules;

/**
 * The initial counts of the molecules parsed from the configuration file.
 */
- (NSArray*)initialcounts;

/**
 * The reaction-channels parsed from the configuration file.
 */
- (NSArray*)reactions;

/**
 * Initialises a new SimulationScript object by parsing a file.
 * Ignores warnings.
 *
 * @param pathToFile path to the file to parse
 * @param error pointer to the error to set if something goes wrong
 * @returns new autoreleased SimulationScript object
 */
- (id)initFromFile:(NSString*)pathToFile
             error:(ParseError**)error;

/**
 * Initialises a new SimulationScript object by parsing a file.
 *
 * @param pathToFile path to the file to parse
 * @param error pointer to the error to set if something goes wrong
 * @param warnings poiner to the warnings to set if something goes wrong
 * @returns new SimulationScript object
 */
- (id)initFromFile:(NSString*)pathToFile
             error:(ParseError**)error
          warnings:(NSMutableArray**)warnings;

/**
 * Creats an autoreleased SimulationScript object by parsing a file.
 * Ignores warnings.
 *
 * @param filepath path to the file to parse
 * @param error pointer to the error to set if something goes wrong
 * @returns new autoreleased SimulationScript object
 */
+ (SimulationScript*)simulationScriptFromFile:(NSString*)filepath
                                        error:(ParseError**)error;

/**
 * Creats an autoreleased SimulationScript object by parsing a file.
 *
 * @param filepath path to the file to parse
 * @param error pointer to the error to set if something goes wrong
 * @param warnings poiner to the warnings to set if something goes wrong
 * @returns new autoreleased SimulationScript object
 */
+ (SimulationScript*)simulationScriptFromFile:(NSString*)filepath
                                        error:(ParseError**)error
                                     warnings:(NSMutableArray**)warnings;

@end
