#import "SimulationScript.h"

@interface SimulationScript(PrivateMethods)

+ (BOOL)stringIsSkipable:(NSString*)string;
+ (BOOL)stringIsStoptimeDefinition:(NSString*)string;
+ (BOOL)stringIsMoleculeDefinition:(NSString*)string;
+ (BOOL)stringIsReactionDefinition:(NSString*)string;
+ (BOOL)stringIsReactionrateDefinition:(NSString*)string;

@end

@implementation SimulationScript(PrivateMethods)

+ (BOOL)stringIsSkipable:(NSString*)string {
    return ([string length] == 0) || [string hasPrefix:COMMENT_START];
}

+ (BOOL)stringIsStoptimeDefinition:(NSString*)string {
    NSString* operator = [VALUE_ASSIGNMENT_OPERATOR escapeForRegex];
    NSString* stoptimeregex =
        [NSString stringWithFormat: @"^t.*%@.*", operator];
    return [string matchesRegex:stoptimeregex];
}

+ (BOOL)stringIsMoleculeDefinition:(NSString*)string {
    NSString* operator = [VALUE_ASSIGNMENT_OPERATOR escapeForRegex];
    NSString* moleculeregex =
        [NSString stringWithFormat:@"^[A-Z].*%@.*", operator];
    return [string matchesRegex:moleculeregex];
}

+ (BOOL)stringIsReactionDefinition:(NSString*)string {
    NSString* operator = [FUNCTION_DECLARATION_OPERATOR escapeForRegex];
    NSString* reactionregex =
        [NSString stringWithFormat:@"^[a-su-z].*%@.*", operator];
    return [string matchesRegex:reactionregex];
}

+ (BOOL)stringIsReactionrateDefinition:(NSString*)string {
    NSString* operator = [VALUE_ASSIGNMENT_OPERATOR escapeForRegex];
    NSString* reactionrateregex =
        [NSString stringWithFormat:@"^[a-su-z].*%@.*", operator];
    return [string matchesRegex:reactionrateregex];
}

@end

@implementation SimulationScript

- (BOOL)isEqual:(id)other {
    if (other == self) {
        return YES;
    }
    if (!other || ![other isKindOfClass:[self class]]) {
        return NO;
    }
    return [_filepath isEqualToString:[other filepath]];
}

+ (SimulationScript*)simulationScriptFromFile:(NSString*)filepath
                                        error:(ParseError**)error {
    SimulationScript* simulationscript =
        [[SimulationScript alloc] initFromFile:filepath error:error];
    return [simulationscript autorelease];
}

+ (SimulationScript*)simulationScriptFromFile:(NSString*)filepath
                                        error:(ParseError**)error
                                     warnings:(NSMutableArray**)warnings {
    SimulationScript* simulationscript =
        [[SimulationScript alloc]
            initFromFile:filepath error:error warnings:warnings];
    return [simulationscript autorelease];
}

- (id)initFromFile:(NSString*)pathToFile error:(ParseError**)error {
    return [self initFromFile:pathToFile error:error warnings:NULL];
}

- (id)initFromFile:(NSString*)filepath
             error:(ParseError**)error
          warnings:(NSMutableArray**)warnings {
    self = [super init];
    if (self == nil) {
        return nil;
    }
    // make sure that the file at filepath exists
    if (![[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
        *error = [ParseError fileNotFound];
        [*error setFile:filepath];
        return nil;        
    }
    _filepath = [filepath retain];
    _filecontents =
        [[NSString stringWithContentsOfFile:filepath
                                   encoding:NSUTF8StringEncoding
                                      error:NULL]
        retain];
    _lines =
        [[_filecontents componentsSeparatedByCharactersInSet:
            [NSCharacterSet newlineCharacterSet]]
        retain];
    // parse contents of file one line at a time
    NSNumber* stoptimetemp = nil;
    NSMutableDictionary* moleculestemp = [NSMutableDictionary dictionary];
    NSMutableArray* moleculesorder = [NSMutableArray array];
    NSMutableDictionary* ratestemp = [NSMutableDictionary dictionary];
    NSMutableDictionary* reactionstemp = [NSMutableDictionary dictionary];
    int lineno = 0;
    while (lineno < [_lines count]) {
        NSString* rawline = [_lines objectAtIndex:lineno];
        lineno++;
        NSString* nowhitespaceline = [rawline removeWhitespace];
        // skip comment-lines and empty lines
        if ([SimulationScript stringIsSkipable:nowhitespaceline]) {
            continue;
        }
        // remove inline comments
        NSString* line = [nowhitespaceline removeAfter:COMMENT_START];
        // determine what the line represents
        // --> line has stop-time (e.g. t = 500)
        if ([SimulationScript stringIsStoptimeDefinition:line]) {
            if (stoptimetemp != nil) {
                *error = [ParseError multipleDeclarationsStoptime];
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            } 
            Assignment* assignment =
                [Assignment parseValueAssignmentFromString:line
                                                 withError:error];
            if (assignment == nil) {
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            stoptimetemp = [assignment expression];
        } 
        // --> line has molecule-count (e.g. X = 100)
        else if ([SimulationScript stringIsMoleculeDefinition:line]) {
            Assignment* assignment =
                [Assignment parseValueAssignmentFromString:line
                                                 withError:error];
            if (assignment == nil) {
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            NSString* moleculename = [assignment variable];
            if ([moleculestemp containsKey:moleculename]) {
                *error = [ParseError multipleDeclarationsMolecule];
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            [assignment setLine:rawline];
            [assignment setLineno:lineno];
            [assignment setFile:filepath];
            [moleculestemp setObject:assignment forKey:moleculename];
            [moleculesorder addObject:moleculename];
        }
        // --> line has reaction-rate (e.g. f = 1.4)
        else if ([SimulationScript stringIsReactionrateDefinition:line]) {
            Assignment* assignment =
                [Assignment parseValueAssignmentFromString:line
                                                 withError:error];
            if (assignment == nil) {
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            NSString* reactionname = [assignment variable];
            if ([ratestemp containsKey:reactionname]) {
                *error = [ParseError multipleDeclarationsReactionrate];
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            [assignment setLine:rawline];
            [assignment setLineno:lineno];
            [assignment setFile:filepath];
            [ratestemp setObject:assignment forKey:reactionname];
        }
        // --> line has reaction definition (e.g. f : E + S -> C)
        else if ([SimulationScript stringIsReactionDefinition:line]) {
            Assignment* assignment =
                [Assignment parseFunctionAssignmentFromString:line
                                                    withError:error];
            if (assignment == nil) {
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            NSString* functionname = [assignment variable];
            if ([reactionstemp containsKey:functionname]) {
                *error = [ParseError multipleDeclarationsReaction];
                [*error setFile:filepath];
                [*error setLine:rawline];
                [*error setLineno:lineno];
                return nil;
            }
            [assignment setLine:rawline];
            [assignment setLineno:lineno];
            [assignment setFile:filepath];
            [reactionstemp setObject:assignment forKey:functionname];
        }
        // --> line is malformed or garbage
        else {
            *error = [ParseError unrecognizedLineType];
            [*error setFile:filepath];
            [*error setLine:rawline];
            [*error setLineno:lineno];
            return nil;
        }
    }
    // make sure stoptime is declared
    NSNumber* stoptime = nil;
    if (stoptimetemp == nil) {
        *error = [ParseError noDeclarationStoptime]; 
        [*error setFile:filepath];
        return nil;
    }
    stoptime = stoptimetemp;
    // make sure at least one molecule is declared
    if ([moleculestemp count] == 0) {
        *error = [ParseError noDeclarationMolecule]; 
        [*error setFile:filepath];
        return nil;
    }
    // check for declared but ununsed molecules
    NSMutableDictionary* moleculesdict = [NSMutableDictionary dictionary];
    for (NSString* molecule in moleculestemp) {
        Assignment* assignment = [moleculestemp objectForKey:molecule];
        BOOL moleculeused = NO;
        for (NSString* key in reactionstemp) {
            NSString* reaction =
                 [[reactionstemp objectForKey:key] expression];
            if ([reaction containsString:molecule]) {
                moleculeused = YES;
                break;
            }
        }
        if (!moleculeused) {
            if (warnings) {
                ParseError* warning = [ParseError moleculeUnused];
                [warning setFile:[assignment file]];
                [warning setLine:[assignment line]];
                [warning setLineno:[assignment lineno]];
                if (*warnings == nil) {
                    *warnings = [NSMutableArray array];
                }
                [*warnings addObject:warning];
            }
            [moleculesorder removeObject:molecule];
        }
        else {
            [moleculesdict setObject:[assignment expression]
                              forKey:[assignment variable]];
        }
    }
    // extract molecule-names and -counts from dictionary
    NSMutableArray* molecules_ = [NSMutableArray array];
    NSMutableArray* initialcounts_ = [NSMutableArray array];
    for (NSString* molecule in moleculesorder) {
        [molecules_ addObject:molecule];
        [initialcounts_ addObject:[moleculesdict objectForKey:molecule]];
    }
    NSArray* molecules = [NSArray arrayWithArray:molecules_];
    NSArray* initialcounts = [NSArray arrayWithArray:initialcounts_];
    // make sure at least one reaction-rate is set
    if ([ratestemp count] == 0) {
        *error = [ParseError noDeclarationReactionrate];
        [*error setFile:filepath];
        return nil;
    }
    // check for declared but unused reaction rates
    NSMutableDictionary* ratesdict = [NSMutableDictionary dictionary];
    for (NSString* kineticconstant in ratestemp) {
        Assignment* assignment = [ratestemp objectForKey:kineticconstant];
        NSString* kineticconstant2 = [kineticconstant lowercaseString];
        BOOL kineticconstantused = NO;
        for (NSString* reactionname in reactionstemp) {
            NSString* reactionname2 = [reactionname lowercaseString]; 
            if ([kineticconstant2 isEqualToString:reactionname2]) {
                kineticconstantused = YES;
                break;
            }
        }
        if (!kineticconstantused) {
            if (warnings) {
                ParseError* warning = [ParseError reactionrateUnused];
                [warning setFile:[assignment file]];
                [warning setLine:[assignment line]];
                [warning setLineno:[assignment lineno]];
                if (*warnings == nil) {
                    *warnings = [NSMutableArray array];
                }
                [*warnings addObject:warning];
            }
        }
        else {
            [ratesdict setObject:[assignment expression]
                          forKey:[assignment variable]];
        }
    }
    // make sure at least some reaction is declared
    if ([reactionstemp count] == 0) {
        *error = [ParseError noDeclarationReaction];
        [*error setFile:filepath];
        return nil;
    }
    NSMutableDictionary* reactionsdict = [NSMutableDictionary dictionary];
    for (NSString* reactionname in reactionstemp) {
        Assignment* assignment = [reactionstemp objectForKey:reactionname];
        // check for reactions without reaction-rates
        NSString* reactionname2 = [reactionname lowercaseString];
        BOOL reactionhasrate = NO;
        for (NSString* rate in ratesdict) {
            NSString* rate2 = [rate lowercaseString];
            if ([reactionname2 isEqualToString:rate2]) {
                reactionhasrate = YES;
                break;
            }
        }
        if (!reactionhasrate) {
            *error = [ParseError reactionWithoutReactionrate];
            [*error setFile:[assignment file]];
            [*error setLine:[assignment line]];
            [*error setLineno:[assignment lineno]];
            return nil;
        }
        // check for reactions using undeclared variables
        NSString* reaction = [assignment expression];
        NSArray* operands =
            [reaction nonEmptyComponentsSeparatedByStringsInArray:
                [NSArray arrayWithObjects:
                    FUNCTION_COMBINATION_OPERATOR,
                    FUNCTION_RESULT_OPERATOR,
                    nil]];
        for (NSString* operand in operands) {
            BOOL operandexists = NO;
            NSString* operand2 = [operand lowercaseString];
            for (NSString* molecule in moleculesdict) {
                NSString* molecule2 = [molecule lowercaseString];
                if ([molecule2 isEqualToString:operand2]) {
                    operandexists = YES;
                    break;
                }
            }
            if (!operandexists) {
                *error = [ParseError reactionWithUndeclaredMolecule];
                [*error setFile:[assignment file]];
                [*error setLine:[assignment line]];
                [*error setLineno:[assignment lineno]];
                return nil;
            }
        }
        [reactionsdict setObject:[assignment expression]
                          forKey:[assignment variable]];
    }
    // extract reactions from dictionaries
    NSMutableArray* mutablereactions =
        [NSMutableArray arrayWithCapacity:[reactionsdict count]];
    for (NSString* name in reactionsdict) {
        NSString* expression = [reactionsdict objectForKey:name];
        double rate = [[ratesdict objectForKey:name] doubleValue];
        Reaction* reaction =
            [Reaction reactionOverAlphabet:molecules
                            withExpression:expression
                                  withRate:rate
                                  withName:name];
        [mutablereactions addObject:reaction];
    }
    NSArray* reactions = [NSArray arrayWithArray:mutablereactions];
    // set fields
    _stoptime = [stoptime retain];
    _molecules = [molecules retain];
    _initialcounts = [initialcounts retain];
    _reactions = [reactions retain];
    return self;
}

// disable default constructor
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    [self release];
    [super doesNotRecognizeSelector:_cmd];
    return nil;
}

- (void)dealloc {
    [_filepath release];
    [_filecontents release];
    [_lines release];
    [_stoptime release];
    [_molecules release];
    [_initialcounts release];
    [_reactions release];
    [super dealloc];
}

+ (void)load {
    [SimulationScript registerSubclass:self];
}

- (NSString*)filepath {
    return _filepath;
}

- (NSArray*)lines {
    return _lines;
}

- (NSNumber*)stoptime {
    return _stoptime;
}

- (NSArray*)molecules {
    return _molecules;
}

- (NSArray*)initialcounts {
    return _initialcounts;
}

- (NSArray*)reactions {
    return _reactions;
}

- (NSString*)filecontents {
    return _filecontents;
}

+ (BOOL)unitTest {
    print(@"Running unit-tests for %s", __FILE__);
    ParseError* error;
    NSMutableArray* warnings;
    error = nil;
    warnings = nil;
    SimulationScript* goodfile =
        [SimulationScript simulationScriptFromFile:@"res/test/allgood.txt"
                                             error:&error
                                          warnings:&warnings];
    // test for bad warning generation
    if (warnings != nil) {
        print(@"--> parsing well formed file raises warnings:");
        for (ParseError* warning in warnings) {
            print(@"----> warning: %@", [warning errorMessage]);
        }
        return NO;
    }
    // test for filepath field
    NSString* filepath = [goodfile filepath];
    NSString* filepathtarget = @"res/test/allgood.txt";
    if (![filepath isEqualToString:filepathtarget]) {
        print(@"--> problem with |_filepath|: "
               "should be %@ but is %@", filepathtarget, filepath);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // test for lines field
    NSString* line = [[goodfile lines] objectAtIndex:2];
    NSString* linetarget = @"t = 200 # inline comments supported";
    if (![line isEqualToString:linetarget]) {
        print(@"--> problem with |_lines| (sample line 3): "
               "should be %@ but is %@", linetarget, line);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // test for stoptime field
    NSNumber* stoptime = [goodfile stoptime];
    NSNumber* stoptimetarget =
        [NSNumber numberWithUnsignedInteger:200];
    if ([stoptime compare:stoptimetarget] != NSOrderedSame) {
        print(@"--> problem with |_stoptime|: "
               "should be %@ but is %@", stoptimetarget, stoptime);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // test for molecules field
    NSArray* molecules = [goodfile molecules];
    NSArray* moleculestarget =
        [NSArray arrayWithObjects: @"E", @"S", @"C", @"P", @"Increasing", nil];
    if (![molecules isEqualToArray:moleculestarget]) {
        print(@"--> problem with |_molecules|: "
               "should be %@ but is %@", moleculestarget, molecules);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // test for initialcounts field
    NSArray* initialcounts = [goodfile initialcounts];
    NSArray* initialcountstarget =
        [NSArray arrayWithObjects:
            [NSNumber numberWithUnsignedInteger:500],
            [NSNumber numberWithUnsignedInteger:500],
            [NSNumber numberWithUnsignedInteger:0],
            [NSNumber numberWithUnsignedInteger:0],
            [NSNumber numberWithUnsignedInteger:0],
            nil];
    if (![initialcounts isEqualToArray:initialcountstarget]) {
        print(@"--> problem with |_initialcounts|: "
               "should be %@ but is %@", initialcountstarget, initialcounts);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // test for reactionrates field
    NSArray* reactions = [goodfile reactions];
    NSArray* alphabet = [goodfile molecules];
    NSArray* reactionstarget =
        [NSArray arrayWithObjects:
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"E+S->C"
                                  withRate:1.0f
                                  withName:@"m"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"C->E+P"
                                  withRate:0.5f
                                  withName:@"n"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"E+E->P"
                                  withRate:0.25f
                                  withName:@"o"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"P+P->E+S"
                                  withRate:0.125f
                                  withName:@"p"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"E->"
                                  withRate:23.0f
                                  withName:@"q"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"P+C->"
                                  withRate:2.3f
                                  withName:@"r"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"->E+C"
                                  withRate:2.3f
                                  withName:@"s"],
            [Reaction reactionOverAlphabet:alphabet
                            withExpression:@"->Increasing"
                                  withRate:2.3f
                                  withName:@"creation"],
            nil];
    if (![[reactions sorted] isEqualToArray:[reactionstarget sorted]]) {
        print(@"--> problem with |_reactions|: "
               "should be %@ but is %@", reactionstarget, reactions);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    BOOL rvalok, errorok, warningsok;
    // non-existing file-paths should give error
    error = nil;
    SimulationScript* noexistfile =
        [[SimulationScript alloc] initFromFile:@"does/not/exist.txt"
                                         error:&error];
    rvalok = noexistfile == nil;
    errorok = [error code] == ParseErrorCode_FileNotFound;
    if (!(rvalok && errorok)) {
        print(@"--> problem with initialising with non-existent path: "
               "should give error with code %d but is %d",
               ParseErrorCode_FileNotFound, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [noexistfile release];
    // lines that are neither value- or reaction-declaration should give error
    error = nil;
    SimulationScript* badlinefile =
        [[SimulationScript alloc] initFromFile:@"res/test/badline.txt"
                                         error:&error];
    rvalok = badlinefile == nil;
    errorok = [error code] == ParseErrorCode_UnrecognizedLineType;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with garbage lines: "
               "should give error with code %d but is %d",
               ParseErrorCode_UnrecognizedLineType, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [badlinefile release];
    // files without stop-time should give error
    error = nil;
    SimulationScript* nostopfile =
        [[SimulationScript alloc] initFromFile:@"res/test/nostoptime.txt"
                                        error:&error];  
    rvalok = nostopfile == nil;
    errorok = [error code] == ParseErrorCode_NoDeclarationStoptime;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files without stop-time: "
               "should give error with code %d but is %d",
               ParseErrorCode_NoDeclarationStoptime, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [nostopfile release];
    // files with multiple stop-time declarations should give error
    error = nil;
    SimulationScript* manystopfile =
        [[SimulationScript alloc] initFromFile:@"res/test/manystoptime.txt"
                                         error:&error];
    rvalok = manystopfile == nil;
    errorok = [error code] == ParseErrorCode_MultipleDeclarationsStoptime;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with multiple stop-time declarations: "
               "should give error with code %d but is %d",
               ParseErrorCode_MultipleDeclarationsStoptime, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [manystopfile release];
    // files with multiple declarations of same molecule should give error
    error = nil;
    SimulationScript* manymoleculefile =
        [[SimulationScript alloc] initFromFile:@"res/test/manymolecule.txt"
                                         error:&error];
    rvalok = manymoleculefile == nil;
    errorok = [error code] == ParseErrorCode_MultipleDeclarationsMolecule;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with multiple molecule declarations: "
               "should give error with code %d but is %d",
               ParseErrorCode_MultipleDeclarationsMolecule, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [manymoleculefile release];
    // files without declarations of molecules should give error
    error = nil;
    SimulationScript* nomoleculefile =
        [[SimulationScript alloc] initFromFile:@"res/test/nomolecule.txt"
                                         error:&error];
    rvalok = nomoleculefile == nil;
    errorok = [error code] == ParseErrorCode_NoDeclarationMolecule;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with missing molecule declarations: "
               "should give error with code %d but is %d",
               ParseErrorCode_NoDeclarationMolecule, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [nomoleculefile release];
    // files with multiple declarations of same reaction-rate should give error
    error = nil;
    SimulationScript* manyreactionratefile =
        [[SimulationScript alloc] initFromFile:@"res/test/manyrates.txt"
                                         error:&error];
    rvalok = manyreactionratefile == nil;
    errorok = [error code] == ParseErrorCode_MultipleDeclarationsReactionrate;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with multiple reaction-rate "
               "declarations: should give error with code %d but is %d",
               ParseErrorCode_MultipleDeclarationsReactionrate, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [manyreactionratefile release];
    // files without declarations of reaction-rates should give error
    error = nil;
    SimulationScript* noreactionratefile =
        [[SimulationScript alloc] initFromFile:@"res/test/norate.txt"
                                         error:&error];
    rvalok = noreactionratefile == nil;
    errorok = [error code] == ParseErrorCode_NoDeclarationReactionrate;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with missing reaction-rate "
               "declarations: should give error with code %d but is %d",
               ParseErrorCode_NoDeclarationReactionrate, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    // files with multiple declarations of same reaction should give error
    error = nil;
    SimulationScript* manyreactionfile =
        [[SimulationScript alloc] initFromFile:@"res/test/manyreactions.txt"
                                         error:&error];
    rvalok = manyreactionfile == nil;
    errorok = [error code] == ParseErrorCode_MultipleDeclarationsReaction;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with multiple reaction declarations: "
               "should give error with code %d but is %d",
               ParseErrorCode_MultipleDeclarationsReaction, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [manyreactionfile release];
    // files without declarations of reactions should give error
    error = nil;
    SimulationScript* noreactionfile =
        [[SimulationScript alloc] initFromFile:@"res/test/noreaction.txt"
                                         error:&error];
    rvalok = noreactionfile == nil;
    errorok = [error code] == ParseErrorCode_NoDeclarationReaction;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with missing reaction declarations: "
               "should give error with code %d but is %d",
               ParseErrorCode_NoDeclarationReaction, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [noreactionfile release];
    // files with reactions without reaction-rates should give error
    error = nil;
    SimulationScript* noratereactionfile =
        [[SimulationScript alloc] initFromFile:@"res/test/noratereaction.txt"
                                         error:&error];
    rvalok = noratereactionfile == nil;
    errorok = [error code] == ParseErrorCode_ReactionWithoutReactionrate;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with reactions with missing rates: "
               "should give error with code %d but is %d",
               ParseErrorCode_ReactionWithoutReactionrate, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [noratereactionfile release];
    // files with reactions using undefined molecules should give error
    error = nil;
    SimulationScript* unknownmoleculefile =
        [[SimulationScript alloc] initFromFile:@"res/test/unknownmolecule.txt"
                                         error:&error];
    rvalok = unknownmoleculefile == nil;
    errorok = [error code] == ParseErrorCode_ReactionWithUndeclaredMolecule;
    if (!(rvalok && errorok)) {
        print(@"--> problem with files with reactions with unknown molecules: "
               "should give error with code %d but is %d",
               ParseErrorCode_ReactionWithUndeclaredMolecule, [error code]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [unknownmoleculefile release];
    // files with unused molecules should give warning
    error = nil;
    warnings = nil;
    NSMutableArray* unusedmoleculewarningstarget =
        [NSMutableArray arrayWithObject:[ParseError moleculeUnused]];
    SimulationScript* unusedmoleculefile =
        [[SimulationScript alloc] initFromFile:@"res/test/unusedmolecule.txt"
                                         error:&error
                                      warnings:&warnings];
    NSArray* filteredmolecules = [unusedmoleculefile molecules];
    NSArray* filteredmoleculesexpected =
        [NSArray arrayWithObjects: @"E", @"S", @"C", @"P", nil];
    rvalok = [filteredmolecules isEqualToArray:filteredmoleculesexpected];
    errorok = error == nil;
    warningsok = [warnings isEqualToArray:unusedmoleculewarningstarget];
    if (!(rvalok && errorok && warningsok)) {
        print(@"--> problem with files with unused molecules:%@%@",
              rvalok ? @"" : [NSString stringWithFormat:
                                @" should return %@ but is %@",
                                filteredmoleculesexpected, filteredmolecules],
              warningsok ? @"" : [NSString stringWithFormat:
                                    @" should give warnings %@ but is %@",
                                    unusedmoleculewarningstarget, warnings]);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [unusedmoleculefile release];
    // files with unused reaction-rates should give warning
    error = nil;
    warnings = nil;
    NSMutableArray* unusedratewarningstarget =
        [NSMutableArray arrayWithObject:[ParseError reactionrateUnused]];
    SimulationScript* unusedratefile =
        [[SimulationScript alloc] initFromFile:@"res/test/unusedrate.txt"
                                         error:&error
                                      warnings:&warnings];
    rvalok = unusedratefile != nil;
    errorok = error == nil;
    warningsok = [warnings isEqualToArray:unusedratewarningstarget];
    if (!(rvalok && errorok && warningsok)) {
        print(@"--> problem with files with unused reaction-rates: "
               "should give warnings %@ but is %@",
               unusedratewarningstarget, warnings);
        print(@"----> last error: %@", [error errorMessage]);
        return NO;
    }
    [unusedratefile release];
    return YES;
}

@end
