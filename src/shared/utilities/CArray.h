#import <Foundation/Foundation.h>

/**
 * A function to test for the equality of two C-arrays of ints.
 *
 * @param a the first array
 * @param aLen the length of the first array
 * @param b the second array
 * @param bLen the length of the second array
 * @returns YES if a and b are equal or NO otherwise
 */
BOOL arraysEqual(int* a, size_t aLen, int* b, size_t bLen);

/**
 * A function to convert a C-array of ints to a string.
 *
 * @param array the array to convert to a string
 * @param len the length of the array
 * @returns an NSString representing the array
 */
NSString* arrayToNSString(int* array, size_t len);

/**
 * A function to make a copy of a C-array of ints.
 *
 * @param array the array to copy
 * @param len the length of the array
 * @returns a pointer to the copied array
 */
int* copyIntArray(int* array, size_t len);

/**
 * A function to make a copy of a C-array of longs.
 *
 * @param array the array to copy
 * @param len the length of the array
 * @returns a pointer to the copied array
 */
long* copyLongArray(long* array, size_t len);
