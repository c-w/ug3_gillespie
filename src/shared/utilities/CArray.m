#import "CArray.h"

BOOL arraysEqual(int* a, size_t aLen, int* b, size_t bLen) {
    if (aLen != bLen) {
        return NO;
    }
    int i = 0;
    while (i < aLen) {
        if (a[i] != b[i]) {
            return NO;
        }
        i++;
    }
    return YES;
}

NSString* arrayToNSString(int* carray, size_t len) {
    NSMutableArray* array = [[NSMutableArray alloc] initWithCapacity:len];
    int i = 0;
    while (i < len) {
        [array addObject:[NSString stringWithFormat:@"%d", carray[i]]];
        i++;
    }
    NSString* arraystring = 
        [NSString stringWithFormat:@"(%@)",
            [array componentsJoinedByString:@", "]];
    [array release];
    return arraystring;
}

int* copyIntArray(int* array, size_t len) {
    int* copy = malloc(sizeof(int) * len);
    if (copy == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    memcpy(copy, array, len * sizeof(int));
    return copy;
}

long* copyLongArray(long* array, size_t len) {
    long* copy = malloc(sizeof(long) * len);
    if (copy == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    memcpy(copy, array, len * sizeof(long));
    return copy;
}
