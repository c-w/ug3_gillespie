#import <Foundation/Foundation.h>

/**
 * Global variable enabling/disabling DLog.
 */
extern BOOL DEBUG;

/**
 * A function to log a statement with debugging information (line, file...).
 * Accepts arguments like NSLog.
 */
#define DLog(fmt, ...)\
    if (DEBUG) {\
        NSLog((@"%s:%d\n" fmt), \
            __FILE__, __LINE__, ##__VA_ARGS__);\
    }

/**
 * Like printf but takes an NSString as argument and adds a newline-character.
 */
void print(NSString* format, ...);
