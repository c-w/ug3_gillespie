#import "IOUtils.h"

void print(NSString* fmt, ...) {
    if (!fmt) {
        return;
    }
    va_list args;
    va_start(args, fmt);
    NSString* string = [[NSString alloc] initWithFormat:fmt arguments:args];
    fputs([string UTF8String], stdout);
    va_end(args);
    [string release];
    fputs("\n", stdout);
}
