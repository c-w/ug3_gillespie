#import <Foundation/Foundation.h>

/**
 * Creates an object that will be released at the end of the current scope.
 *
 * @see http://kickingbear.com/blog/archives/13
 */
#define ScopeReleased __attribute__((cleanup(_$scopeReleaseObject)))

/**
 * Creates a new auto-release pool and drains it at the end of the current
 * scope.
 *
 * @see http://kickingbear.com/blog/archives/13
 */
#define ScopeAutoreleased()\
    NSAutoreleasePool* _$autoreleasePool##__LINE__ \
        __attribute__((cleanup(_$scopeDrainAutoreleasePool))) =\
        [[NSAutoreleasePool alloc] init]

/**
 * Helper function for the ScopeReleased macro.
 */
void _$scopeReleaseObject(id* scopeReleasedObject);

/**
 * Helper function for the ScopeAutoreleased() macro.
 */
void _$scopeDrainAutoreleasePool(NSAutoreleasePool** scopeReleasedPool);
