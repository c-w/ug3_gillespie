#import "MemoryUtils.h"

void _$scopeReleaseObject(id* object) {
    [*object release];
    *object = nil;
}

void _$scopeDrainAutoreleasePool(NSAutoreleasePool** pool) {
    [*pool drain];
    [*pool release];
    *pool = nil;
}
