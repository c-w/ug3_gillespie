#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSArray.
 *
 * @category NSArray(Utilities)
 */
@interface NSArray(Utilities)

/**
 * Creates a new NSArray by reading the contents of a file.
 * Each line in the file is an element of the new array.
 *
 * @param path the path to read from
 * @returns an array containing the lines in the file
 */
+ (NSArray*)arrayWithLinesInFile:(NSString*)path;

/**
 * Creates a sorted copy of the invoking array.
 *
 * @returns a sorted copy
 */
- (NSArray*)sorted;

/**
 * Converts the invoking array to a C-array of ints.
 *
 * @returns a C-array of ints
 */
- (int*)toIntArray;

/**
 * Converts the invoking array to a C-array of longs.
 *
 * @returns a C-array of longs
 */
- (long*)toLongArray;

/**
 * Converts a C-array of ints to an NSArray.
 *
 * @param carray a pointer to a C-array of ints
 * @param len the length of the C-array
 * @returns an array with the contents of the C-array
 */
+ (NSArray*)arrayFromIntArray:(int*)carray ofLength:(size_t)len;

/**
 * Converts a C-array of longs to an NSArray.
 *
 * @param carray a pointer to a C-array of longs
 * @param len the length of the C-array
 * @returns an array with the contents of the C-array
 */
+ (NSArray*)arrayFromLongArray:(long*)carray ofLength:(size_t)len;

@end
