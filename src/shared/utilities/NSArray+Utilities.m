#import "NSArray+Utilities.h"

@implementation NSArray(Utilities)

+ (NSArray*)arrayWithLinesInFile:(NSString*)path {
    // read content of file at path into a single string
    NSString* filecontents =
        [[NSString alloc] initWithContentsOfFile:path
                                        encoding:NSUTF8StringEncoding
                                           error:NULL];
    // split string into lines
    NSArray* lines =
        [filecontents componentsSeparatedByCharactersInSet:
            [NSCharacterSet newlineCharacterSet]];
    [filecontents release];
    return lines;
}

- (NSArray*)sorted {
    NSArray* sortedarray = [self sortedArrayUsingSelector:@selector(compare:)];
    return sortedarray;
}

- (int*)toIntArray {
    int* carray = (int*)malloc(sizeof(int) * [self count]);
    if (carray == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    int i = 0;
    for (NSNumber* element in self) {
        carray[i] = [element intValue];
        i++;
    }
    return carray;
}

- (long*)toLongArray {
    long* carray = (long*)malloc(sizeof(long) * [self count]);
    if (carray == NULL) {
        fprintf(stderr, "critical error: malloc failed in function %s\n",
                __FUNCTION__);
        exit(EXIT_FAILURE);
    }
    int i = 0;
    for (NSNumber* element in self) {
        carray[i] = [element longValue];
        i++;
    }
    return carray; 
}

+ (NSArray*)arrayFromIntArray:(int*)carray ofLength:(size_t)len {
    NSMutableArray* mutablearray =
        [[NSMutableArray alloc] initWithCapacity:len];
    int i = 0;
    while (i < len) {
        NSNumber* num = [[NSNumber alloc] initWithInt:carray[i]];
        [mutablearray addObject:num];
        [num release];
        i++;
    }
    NSArray* array = [NSArray arrayWithArray:mutablearray];
    [mutablearray release];
    return array;
}

+ (NSArray*)arrayFromLongArray:(long*)carray ofLength:(size_t)len {
    NSMutableArray* mutablearray =
        [[NSMutableArray alloc] initWithCapacity:len];
    int i = 0;
    while (i < len) {
        NSNumber* num = [[NSNumber alloc] initWithLong:carray[i]];
        [mutablearray addObject:num];
        [num release];
        i++;
    }
    NSArray* array = [NSArray arrayWithArray:mutablearray];
    [mutablearray release];
    return array;
}

@end
