#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSDictionary.
 *
 * @category NSDictionary(Utilities)
 */
@interface NSDictionary(Utilities)

/**
 * Tests to see if a given key is in the invoking dictionary.
 *
 * @param key the key to search for
 * @returns YES if the key is in the dictionary or NO otherwise
 */
- (BOOL)containsKey:(id)key;

/**
 * Creates a NSArray of the keys of the invoking dictionary and sorts them.
 *
 * @returns a sorted array of keys of the dictionary
 */
- (NSArray*)sortedKeys;

/**
 * Creates a NSArray of the values of the invoking dictionary and sorts them 
 * by their keys.
 *
 * @returns a sorted array of the values in the dictionary
 */
- (NSArray*)valuesSortedByKeys;

@end
