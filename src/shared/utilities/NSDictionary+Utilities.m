#import "NSDictionary+Utilities.h"

@implementation NSDictionary(Utilities)

- (BOOL)containsKey:(id)key {
    return [self objectForKey:key] != nil;
}

- (NSArray*)sortedKeys {
    NSArray* sortedkeys =
        [[self allKeys] sortedArrayUsingSelector:@selector(compare:)];
    return sortedkeys;
}

- (NSArray*)valuesSortedByKeys {
    NSArray* sortedvalues =
        [self objectsForKeys:[self sortedKeys] notFoundMarker:NULL];
    return sortedvalues;
}

@end
