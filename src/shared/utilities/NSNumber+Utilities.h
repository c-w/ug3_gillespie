#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSNumber.
 *
 * @category NSNumber(Utilities)
 */
@interface NSNumber(Utilities)

/**
 * Creats a new NSNumber from a string.
 *
 * @param string the string to convert into a number
 * @returns a NSNumber corresponding parsed from the string
 */
+ (NSNumber*)numberWithString:(NSString*)string;

/**
 * Creates a NSNumber with value of 0.
 *
 * @returns an autoreleased NSNumber representing 0
 */
+ (NSNumber*)zero;

@end
