#import "NSNumber+Utilities.h"

@implementation NSNumber(Utilities)

+ (NSNumber*)numberWithString:(NSString*)string {
    NSNumber* number = [NSNumber numberWithDouble:[string doubleValue]];
    return number;
}

+ (NSNumber*)zero {
    NSNumber* zero = [NSNumber numberWithInt:0];
    return zero;
}

@end
