#import <Foundation/Foundation.h>

/**
 * Convenience functions on NSString.
 *
 * @category NSString(Utilities)
 */
@interface NSString(Utilities)

/**
 * Creates a new autoreleased string from the invoking string in form
 * |oldstring|prepend|padstring*padAmount|.
 *
 * @param prepend the string to put in between the old string and the padding
 * @param padamount the number of replications of padstring
 * @param padstring the string to replicate and pad with
 * @returns the padded and prepended string
 */
- (NSString*)stringByPaddingRightAndPrepending:(NSString*)prepend
                                     padAmount:(int)padamount
                                   withString:(NSString*)padstring;

/**
 * Tests if the invoking string represents a number.
 *
 * @returns YES if the string can be parsed as a number or NO otherwise
 */
- (BOOL)isNumeric;

/**
 * Tests if the invoking string matches a regular expression.
 *
 * @param regex a string containing the regular expression to match
 * @returns YES if the string matches the regex or no otherwise
 */
- (BOOL)matchesRegex:(NSString*)regex;

/**
 * Tests if the invoking string contains a substring.
 *
 * @param substring the substring to find
 * @returns YES if the string contains the substring or NO otherwise
 */
- (BOOL)containsString:(NSString*)substring;

/**
 * Removes all whitespace from the invoking string.
 *
 * @returns a copy without whitespace
 */
- (NSString*)removeWhitespace;

/**
 * Escapes the invoking string for safe use within regular-expressions.
 *
 * @returns a copy with special regular-expression characters escaped
 */
- (NSString*)escapeForRegex;

/**
 * Removes anything after some delimiter in the invoking string.
 *
 * @param delim the delimiter to remove after
 * @returns a copy with everything after the delimiter removed
 */
- (NSString*)removeAfter:(NSString*)delim;

/**
 * Splits the invoking string into substrings delimited by a separator.
 * Removes all empty substrings resulting from this operation.
 *
 * @param separator the separator string
 * @returns an array of components
 */
- (NSArray*)nonEmptyComponentsSeparatedByString:(NSString*)separator;

/**
 * Splits the invoking string into substrings delimited by separators.
 *
 * @param separators an array of separators
 * @returns an array of components
 */
- (NSArray*)componentsSeparatedByStringsInArray:(NSArray*)separators;

/**
 * Splits the invoking string into substrings delimited by separators.
 * Removes all empty substrings resulting from this operation.
 *
 * @param separators an array of separators
 * @returns an array of components
 */
- (NSArray*)nonEmptyComponentsSeparatedByStringsInArray:(NSArray*)separators;

/**
 * Writes the contents of the invoking string to a file.
 *
 * @param filepath the path to write to
 */
- (void)writeToPath:(NSString*)filepath;

/**
 * Replaces the extension of the invoking string with a new extension.
 *
 * @param extension the new extenstion
 * @returns a new string with different extension
 */
- (NSString*)stringByReplacingExtensionWithString:(NSString*)extension;

@end
