#import "NSString+Utilities.h"

@implementation NSString(Utilities)

- (NSString*)stringByPaddingRightAndPrepending:(NSString*)prepend
                                     padAmount:(int)padamount
                                    withString:(NSString*)padstring {
    if (padamount <= 0) {
        return self;
    }
    NSString* padding =
        [[NSString string] stringByPaddingToLength:padamount
                                        withString:padstring
                                   startingAtIndex:0];
    NSString* prependstring = prepend ? prepend : @"";
    NSString* paddedstring =
        [self stringByAppendingFormat:@"%@%@", prependstring, padding];
    return paddedstring;
}

- (BOOL)isNumeric {
    return [[NSScanner scannerWithString:self] scanFloat:NULL];
}

- (BOOL)matchesRegex:(NSString*)regex {
    return
        [[NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex]
            evaluateWithObject:self];
}

- (BOOL)containsString:(NSString*)string {
    return [self rangeOfString:string].location != NSNotFound;
}

- (NSString*)removeWhitespace {
    NSArray* tokens =
        [self componentsSeparatedByCharactersInSet:
            [NSCharacterSet whitespaceAndNewlineCharacterSet]];
    NSString* nospacestring = [tokens componentsJoinedByString:@""];
    return nospacestring;
}

- (NSString*)escapeForRegex {
    NSMutableArray* chararray = [[NSMutableArray alloc] init];
    NSInteger chrIndex = 0;
    while (chrIndex < [self length]) {
        unichar chr = [self characterAtIndex:chrIndex];
        if (chr == '*' || chr == '?' || chr == '+' || chr == '[' || 
            chr == ']' || chr == '(' || chr == ')' || chr == '{' ||
            chr == '^' || chr == '$' || chr == '|' || chr == 92  ||
            chr == '.' || chr == '/') {
            // 92 is the backslash character '\'
            [chararray addObject:[NSString stringWithFormat:@"%c", 92]];
        }
        [chararray addObject:[NSString stringWithFormat:@"%c", chr]];
        chrIndex++;
    }
    NSString* escapedstring = [chararray componentsJoinedByString:@""];
    return escapedstring;
}

- (NSString*)removeAfter:(NSString*)delim {
    NSString* truncatedstring =
        [[self componentsSeparatedByString:delim] objectAtIndex:0];
    return truncatedstring;
}

- (NSArray*)nonEmptyComponentsSeparatedByString:(NSString*)separator {
    NSArray* components =
        [[self componentsSeparatedByString:separator]
                    filteredArrayUsingPredicate:
                        [NSPredicate predicateWithFormat:@"length > 0"]];
    return components;
}

- (NSArray*)nonEmptyComponentsSeparatedByStringsInArray:(NSArray*)separators {
    NSArray* components =
        [[self componentsSeparatedByStringsInArray:separators]
                    filteredArrayUsingPredicate:
                        [NSPredicate predicateWithFormat:@"length > 0"]];
    return components;
}

- (NSArray*)componentsSeparatedByStringsInArray:(NSArray*)separators {
    NSMutableArray* componentsmutable = [NSMutableArray array];
    NSInteger separatorsIndex = 0;
    while (separatorsIndex < [separators count]) {
        NSString* separator = [separators objectAtIndex:separatorsIndex];
        if ([componentsmutable count] == 0) {
            if ([self containsString:separator]) {
                NSArray* tokens = [self componentsSeparatedByString:separator];
                [componentsmutable addObjectsFromArray:tokens];
            }
        }
        else {
            NSInteger componentsIndex = 0;
            while (componentsIndex < [componentsmutable count]) {
                NSString* string =
                    [componentsmutable objectAtIndex:componentsIndex];
                if ([string containsString:separator]) {
                    NSArray* substrings =
                        [string componentsSeparatedByString:separator];
                    [componentsmutable removeObjectAtIndex:componentsIndex];
                    [componentsmutable addObjectsFromArray:substrings];
                }
                componentsIndex++;
            }
        }
        separatorsIndex++;
    }
    NSArray* components = [NSArray arrayWithArray:componentsmutable];
    return components;
}

- (void)writeToPath:(NSString*)filepath {
    // make sure we can actually write to the path
    NSString* basepath = [filepath stringByDeletingLastPathComponent];
    [[NSFileManager defaultManager] createDirectoryAtPath:basepath
                              withIntermediateDirectories:YES
                                               attributes:nil
                                                    error:NULL];
    NSError* error = nil;
    [self writeToFile:filepath
           atomically:NO
             encoding:NSUTF8StringEncoding
                error:&error];
    if (error != nil) {
        fprintf(stderr, "critical error: writing to %s failed (%s)\n",
                [filepath UTF8String],
                [[error localizedDescription] UTF8String]);
        exit(EXIT_FAILURE);
    }
}

- (NSString*)stringByReplacingExtensionWithString:(NSString*)extension {
        NSString* basepath = [self stringByDeletingLastPathComponent];
        NSString* filename =
            [[self lastPathComponent] stringByDeletingPathExtension];
        NSString* newstring =
            [basepath stringByAppendingPathComponent:
                [NSString stringWithFormat:@"%@.%@", filename, extension]];
        return newstring;
}

@end
