#import <Foundation/Foundation.h>

/**
 * A function to return a random float that is contained in some interval.
 *
 * @param low the lower bound of the random number
 * @param high the upper bound of the random number
 * @returns a random number chosen uniformly over [low:high]
 */
float random_float(float low, float high);

/**
 * A function to return a random double that is contained in some interval.
 *
 * @param low the lower bound of the random number
 * @param high the upper bound of the random number
 * @returns a random number chosen uniformly over [low:high]
 */
double random_double(double low, double high);
