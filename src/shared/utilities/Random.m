#import "Random.h"

float random_float(float low, float high) {
    return (((float)rand() / RAND_MAX) * (high - low)) + low;
}

double random_double(double low, double high) {
    return (((double)rand() / RAND_MAX) * (high - low)) + low;
}
