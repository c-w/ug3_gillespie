#import <Foundation/Foundation.h>

/**
 * A subclass of NSObject to represent an object that can be tested.
 */
@interface TestableObject : NSObject

/**
 * Registers a subclass of TestableObject.
 * Subclasses of TestableObject should call this method.
 * This allows for easy iteration over all subclasses of TestableObject
 * e.g. to programmatically invoke some method on all subclasses.
 *
 * @param subclass the subclass to register
 */
+ (void)registerSubclass:(Class)subclass;

/**
 * Method to run all unit-tests for the class.
 * Should be implemented by subclasses of TestableObject.
 *
 * @returns YES if all the unit-tests of the class have passed or NO otherwise
 */
+ (BOOL)unitTest;

/**
 * Returns a list of all currently registered subclasses of TestableObject.
 *
 * @returns an array of subclasses of NSObject
 */
+ (NSArray*)registeredSubclasses;

@end
