#import "TestableObject.h"

@implementation TestableObject

static NSMutableArray* registeredSubclasses;

+ (void)registerSubclass:(Class)subclass {
    if (registeredSubclasses == nil) {
        registeredSubclasses = [[NSMutableArray alloc] init];
    }
    [registeredSubclasses addObject:subclass];
}

+ (NSArray*)registeredSubclasses {
    NSArray* subclasses = [NSArray arrayWithArray:registeredSubclasses];
    return subclasses;
}

+ (BOOL)unitTest {
    NSAssert(NO, @"subclasses need to overwrite this method");
    return NO;
}

@end
